<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', ['class' => 'common\components\LangSelector']],
    'controllerNamespace' => 'frontend\controllers',
    'layout' => 'inner',
    'components' => [
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
//            'currencyCode' => 'EUR',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
//                'domain' => 'new.kicapastry.dev',
                'domain' => $_SERVER['HTTP_HOST'],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'identityCookie' => [
//                'name' => '_identity-frontend',
//                'httpOnly' => true,
//                'path' => '/',
//                'domain' => 'new.kicapastry.dev',
//            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'cookieParams' => [
                'path' => '/',
//                'domain' => 'new.kicapastry.dev',
                'domain' => $_SERVER['HTTP_HOST'],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
//                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                // TODO fix this shit
                '<language:(|en|es)>'                                                           => 'site/index',
                ''                                                                              => 'site/index',
                '<language:(en|es)>/<controller:(chef|recipe|faq|gallery|favorite|cart)>'       => '<controller>/index',
                '<controller:(chef|recipe|faq|gallery|favorite|cart)>'                          => '<controller>/index',
                '<language:(en|es)>/<controller:(contact|about|policy)>'                        => 'site/<controller>',
                '<controller:(contact|about|policy)>'                                           => 'site/<controller>',
                '<language:(en|es)>/<controller:broadcast>'                                     => 'course/<controller>',
                '<controller:broadcast>'                                                        => 'course/<controller>',
                '<language:(en|es)>/<controller:course>/<action:online>/<attr>/<value>'         => '<controller>/<action>',
                '<controller:course>/<action:online>/<attr:\w+>/<value:\d+>'                    => '<controller>/<action>',
                '<language:(en|es)>/<controller:course>/<action:(online|subscribe)>'            => '<controller>/<action>',
                '<controller:course>/<action:(online|subscribe)>'                               => '<controller>/<action>',
                '<language:(en|es)>/<controller:course>/<action:favorite>/<id>'                 => '<controller>/<action>',
                '<controller:course>/<action:favorite>/<id>'                                    => '<controller>/<action>',
                '<language:(en|es)>/<controller:(course|chef)>/<url>'                           => '<controller>/view',
                '<controller:(course|chef)>/<url>'                                              => '<controller>/view',
                '<language:(en|es)>/<controller:user>'                                          => '<controller>/index',
                '<controller:user>'                                                             => '<controller>/index',
                '<language:(en|es)>/<controller:user>/<action:(index|settings|logout|login|request-password-reset|signup)>'   => '<controller>/<action>',
                '<controller:user>/<action:(index|settings|logout|login|request-password-reset|signup)>'                      => '<controller>/<action>',
                '<language:(en|es)>/<controller:cart>/<action:payment>/<post_action>'           => '<controller>/<action>',
                '<controller:cart>/<action:payment>/<post_action>'                              => '<controller>/<action>',
                '<language:(en|es)>/<controller:(cart|favorite)>/<action>/<id>'                 => '<controller>/<action>',
                '<controller:(cart|favorite)>/<action>/<id>'                                    => '<controller>/<action>',
                '<language:(en|es)>/<controller:(recipe|gallery)>/<id>'                         => '<controller>/view',
                '<controller:(recipe|gallery)>/<id>'                                            => '<controller>/view',
                '<language:(en|es)>/<controller:cart>/<action:(payment|discount)>'                         => '<controller>/<action>',
                '<controller:cart>/<action:(payment|discount)>'                                            => '<controller>/<action>',
                '<controller:order>/<action:success>'                                           => '<controller>/<action>',

//                '<language:(|en|es)>/<action:(contact|about|policy)>'                                   => 'site/<action>',
//                '<language:(|en|es)>/<controller:course>/<action:(online|search|favorite|broadcast)>'   => '<controller>/<action>',
//                '<language:(|en|es)>/<controller:(chef|course)>/<action:index>'                         => '<controller>/index',
//                '<language:(|en|es)>/<controller:(chef|course)>/<id>'                                   => '<controller>/view',
//                '<language:(|en|es)>/<controller>/<id:\d+>'                                             => '<controller>/view',
//                '<language:(|en|es)>/<controller>/<action>'                                             => '<controller>/<action>',
//                '<language:(|en|es)>/<controller>/<action>/<id:\d+>'                                    => '<controller>/<action>',
//                '<language:(|en|es)>/<controller>/<action>/<attr>/<value>'                              => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        'cart'          => 'common\controllers\CartController',
        'recipe'        => 'common\controllers\RecipeController',
        'faq'           => 'common\controllers\FaqController',
        'gallery'       => 'common\controllers\GalleryController',
        'favorite'      => 'common\controllers\FavoriteController',
        'order'         => 'common\controllers\OrderController',
    ]
];
