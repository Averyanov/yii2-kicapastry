jQuery('ul.nav li.dropdown').hover(function() {
	jQuery(this).find('.dropdown-menu').stop(true, true).show();
	jQuery(this).addClass('open');
}, function() {
jQuery(this).find('.dropdown-menu').stop(true, true).hide();
	jQuery(this).removeClass('open');
	$(".popover").css("display", "none");
});



$.getScript("/js/jquery.sharrre.min.js", function(){
	
	$('#twitter').sharrre({
	  share: {
	    twitter: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('twitter');
	  }
	});
	
	$('#facebook').sharrre({
	  share: {
	    facebook: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('facebook');
	  }
	});
	
	
	$('#pinterest').sharrre({
	  share: {
	    pinterest: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('pinterest');
	  }
	});
	
	
		
	$('#googleplus').sharrre({
	  share: {
	    googlePlus: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('googlePlus');
	  }
	});
	
	
	$('#comment').sharrre({
	  share: {
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	  	window.location = "#comments";
	  }
	});
	
	
	$('#views').sharrre({
	  share: {
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	  	window.location = "#";
	  }
	});
	
	
	$('#shares').sharrre({
	  share: {
	  },
	  enableHover: false,
	  enableTracking: true,
	  click: function(api, options){
	  	window.location = "#";
	  }
	});

});

// function removeP(){
//     $('.shtora p ul').remove();
//     $('.shtora').css('display','none');
// }

 singnvalidator = {
    "mail": false,
    "phone": false,
    "name": false,
	'country':'Ukraine'
};
function aproveFormData(){
	validator.aprove = true;
   $('form[name=fsignIn]').submit();
}
function checkFirstForm() {
    $('.shtora p ul').remove();
	if ($("#agreement").prop("checked")) {

		var name = $('#fiName').val();
		var tel = $('#fiTel').val();
		var email = $('#fiEmail').val();

		if(name.length > 0 && tel.length > 0 && email.length > 0) {

			if(singnvalidator.mail && singnvalidator.phone && singnvalidator.name) {
				if(lang == 'ru') {
                    $('.shtora p').append(
                        "<ul>" +
                        "<li>Ваша страна : <b>" + phone_country + "</b></li>" +
                        "<li>Имя : <b>" + name + "</b></li>" +
                        "<li>Телефон : <b>" + tel + "</b></li>" +
                        "<li>Email : <b>" + email + "</b></li>" +
                        "<li>Правильно ли заполненые данные ?</li>" +
                        "</ul>"
                    );
                }
                if(lang == 'en') {
                    $('.shtora p').append(
                        "<ul>" +
                        "<li>Country : <b>" + phone_country + "</b></li>" +
                        "<li>Name : <b>" + name + "</b></li>" +
                        "<li>Phone number : <b>" + tel + "</b></li>" +
                        "<li>Email : <b>" + email + "</b></li>" +
                        "<li>Have all data been entered correctly ?</li>" +
                        "</ul>"
                    );
                }
                if(lang == 'es') {
                    $('.shtora p').append(
                        "<ul>" +
                        "<li>Country : <b>" + phone_country + "</b></li>" +
                        "<li>Name : <b>" + name + "</b></li>" +
                        "<li>Phone number : <b>" + tel + "</b></li>" +
                        "<li>Email : <b>" + email + "</b></li>" +
                        "<li>Have all data been entered correctly ?</li>" +
                        "</ul>"
                    );
                }

                $('form[name=mirrow] input[name=name]').val(name);
                $('form[name=mirrow] input[name=tel]').val(tel);
                $('form[name=mirrow] input[name=email]').val(email);

                $('.shtora').css('display', 'block');
            }
		}
		if(name.length == 0 || name.length < 2){
			$('#fiName').css('border','1px solid red');
            singnvalidator.name = false;
		}
		else{
			$(this).css('border','1px solid #ccc');
            singnvalidator.name  = true;
		}
		if(tel.length == 0 || tel.length<10 || tel.length>13){
			$('#fiTel').css('border','1px solid red');
            singnvalidator.phone = false;
		}
		else{
			$('#fiTel').css('border','1px solid #ccc');
            singnvalidator.phone = true;
		}
		if(email.length == 0){
			$('#fiEmail').css('border','1px solid red');
            singnvalidator.mail = false;
		}
		else{
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test(email)){
                $('#fiEmail').css('border','1px solid #ccc');
                singnvalidator.mail = true;
            }else{
                singnvalidator.mail = false;
			}

		}
	}
}
function backFirstForm(){
	$('.shtora p ul').remove();
	$('.shtora').css('display', 'none');
}

$( document ).ready(function() {

    // $('input[name=tel]').val(phone_pref);
	var loginValidator = false;

    $('.formBlockGate a').bind( "click", function() {
         var model = this;
         if($(model).attr('name') != 'login' && $(model).attr('name') != 'fsignin'){

         	if($(model).attr('name') == 'goto'){
         		return true;
			}
         	return false;
		 }
         $('.formBlockGate a').removeClass('active');
         $(model).addClass('active');
         if($(model).attr('name') == 'login'){
             $('form[name=logIn]').removeClass('hidden');
             $('form[name=fsignIn]').addClass('hidden');
             $('.shtora').css('display', 'none');
         }
         else if($(model).attr('name') == 'fsignin'){

             $('form[name=fsignIn]').removeClass('hidden');
             $('form[name=logIn]').addClass('hidden');
         }

    });
	$('form[name=fsignIn] input[name=agreement]').on('change',function(){

		if (!this.checked) {
			$('#fakebutton').attr('disabled', true);
		}
		else{
			$('#fakebutton').attr('disabled', false);
		}
	});

    $('form[name=logIn]').submit(function(e){

    	if(!loginValidator) {
            e.preventDefault();
            console.log("error");
        }

	});

    $('#fiName').keyup(function() {
        if($( this ).val().length < 2){
            $(this).css('border','1px solid red');
            singnvalidator.name = false;
        }
        else{
            $(this).css('border','1px solid #ccc');
            singnvalidator.name = true;
        }
    })
    $('#fiTel').keyup(function() {
       if($( this ).val().length < 10 || $( this ).val().length > 13 ){
           $(this).css('border','1px solid red');
           singnvalidator.phone = false;
       }
       else{
           $(this).css('border','1px solid #ccc');
           singnvalidator.phone = true;
       }
    }).keydown(function(e){
        var key = e.charCode || e.keyCode || 0;
		return (
			key == 8 ||
			key == 9 ||
			key == 13 ||
			key == 46 ||
			key == 110 ||
			key == 190 ||
			(key >= 35 && key <= 40) ||
			(key >= 48 && key <= 57) ||
			(key >= 96 && key <= 105));
	});
    $('form[name=fsignIn] input[name="email"]').blur(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){
          //      $('#valid').text('Верно');
				$(this).css('border','1px solid #ccc');
                // $('div.warning').removeClass('hidden');
                singnvalidator.mail = true;
            } else {
                $(this).css('border','1px solid red');
             //   $('#valid').text('Не верно');
                singnvalidator.mail = false;
              //   $('div.warning').removeClass('hidden');
            }
        } else {
            $(this).css('border','1px solid #ccc');
            singnvalidator.mail = false;
            // $('#valid').text('Поле email не должно быть пустым');
            // $('div.warning').removeClass('hidden');
        }
    });
    $('form[name=logIn] input[name="email"]').blur(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){
                //      $('#valid').text('Верно');
                $(this).css('border','1px solid #ccc');
                loginValidator = true;
            } else {
                $(this).css('border','1px solid red');
                //   $('#valid').text('Не верно');
                loginValidator = false;
            }
        } else {
            $(this).css('border','1px solid #ccc');
            loginValidator = false;
            // $('#valid').text('Поле email не должно быть пустым');
        }
    });
	 //submit

	 $('#sBlock').click(function() {
	 	var link = $(this).children("a");
		window.location.replace(link.attr("href"));
	 });
	 
	// $('[data-toggle="tooltip"]').tooltip();

	$("text").dblclick(function(){
		window.location.replace("/admin/?module=site_settings&item_id="+$(this).attr("id")+"&edit=edit");
	});
	$("textblock").dblclick(function(){
		var link = $(this).children("mark").children("a");
		window.location.replace(link.attr("href"));
	});
						
	$(".online_line").click(function(e){
		e.preventDefault();
		e.stopPropagation();
       // var request = {};
       //     request['header_line_close'] = 1;
	    //    $.ajax({
       //              type: 'POST',
       //              url: "close_header_line.php",
       //              dataType: 'json',
       //              cache: false,
       //              data: request,
       //              success: function(data) {
       //                  if (data.success =='1') {
       //                       $(".online_line").hide();
       //                  }
       //              }
       //          });
        $(".off_line_link").click();
        return false;
	});
    				
	$(".line_close").click(function(){
        $(this).parent().hide('slow');
        // var request = {};
       //     request['header_line_close'] = 1;
	    //    $.ajax({
       //              type: 'POST',
       //              url: "/close_header_line.php",
       //              dataType: 'json',
       //              cache: false,
       //              data: request,
       //              success: function(data) {
       //                  if (data.success =='1') {
       //                       $(".online_line").css('display','none');
       //                  }
       //              }
       //          });
        return false;
	});
    		
	
//	var menu = $('.navbar-kica');
//	var origOffsetY = menu.offset().top;
//	function scroll() {
//		
//		
//		if ($(window).scrollTop() >= origOffsetY) {
//		    $('.navbar-kica').addClass('navbar-fixed-top');
//		} else {
//		    $('.navbar-kica').removeClass('navbar-fixed-top');
//		}
//	}
//	document.onscroll = scroll;
    if(document.cookie.indexOf('jump') == -1)
    {
        popup.classList.remove('hidden');
        bg.classList.remove('hidden');
    }
    more_button.addEventListener('click', function () {
        step_first.classList.add('hidden');
        step_second.classList.remove('hidden');
    });

    close_button.addEventListener('click', function () {
        popup.classList.add('hidden');
        bg.classList.add('hidden');
    })


	
	
});


$.getScript("/js/jquery.fancybox.min.js", function(){
    // main
	$('[rel="galleryBox"]').fancybox({
    		'transitionIn'	:	'elastic',
    		'transitionOut'	:	'elastic',
    		'speedIn'		:	600, 
    		'speedOut'		:	200, 
    		'overlayShow'	:	true,
            'overlayOpacity':   0.2
    	});
});



// $.getScript("/include/js/bootstrap-multiselect.js", function(){
// });

function onFrameLoadedYtb()
{
	$(".ytp-popup").hide();
}
var validator={
	"email": false,
	"tel": false,
	"name": false,
	"sname": false,
	"city": false,
	"text": false
};

$('form.form-horizontal input,form.form-horizontal textarea ').blur(function() {
	var get_name ;
	get_name = $(this).attr('name');
	if($(this).attr('name') != 'email'){
		if($(this).val() != '') {
			$(this).css('border','1px solid #ccc');
			$('div.warning').removeClass('hidden');
			validator[get_name] = true;

		} else {
			$(this).css('border','1px solid red');
			$('input[name="submit_feedback"]').attr("disabled", "disabled");
			validator[get_name] = false;
		}
	}
	else{
		if($(this).attr('name') == 'email') {
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if(pattern.test($(this).val())){
				//      $('#valid').text('Верно');
				$(this).css('border','1px solid #ccc');
				$('div.warning').removeClass('hidden');
				validator.email = true;

			} else {
				$(this).css('border','1px solid red');
				//   $('#valid').text('Не верно');
				$('input[name="submit_feedback"]').attr("disabled", "disabled");
				validator.email = false;
				//$('input[name="submit_feedback"]').removeAttr('disabled');
			}
		} else {
			$(this).css('border','1px solid #ccc');
			$('input[name="submit_feedback"]').attr("disabled", "disabled");
			validator.email = false;
			// $('#valid').text('Поле email не должно быть пустым');
		}
	}
	var count_true = 0;
	for (var key in validator) {
		if (validator[key] != false){
			count_true++;
		}
	}
	if (count_true == 6){
		$('input[name="submit_feedback"]').removeAttr('disabled');
	}
});

$('input[name="tel"]').keyup(function() {
	if($( this ).val().length < 10 || $( this ).val().length > 12 ){
		$('#iTel').css('border','1px solid red');
		validator.phone = false;
	}
	else{
		$('#iTel').css('border','1px solid #ccc');
		validator.phone = true;
	}

}).keydown(function(e){
	var key = e.charCode || e.keyCode || 0;
	return (
	key == 8 ||
	key == 9 ||
	key == 13 ||
	key == 46 ||
	key == 110 ||
	key == 190 ||
	(key >= 35 && key <= 40) ||
	(key >= 48 && key <= 57) ||
	(key >= 96 && key <= 105));
});

function check_valid(f)
{
	if((f.email.value=="") || (f.name['name'].value=="") || (f.sname.value=="") || (f.tel.value=="") || (f.city.value.trim()=="") || (f.text.value.trim()=="") )
	{
		var msgElem = document.createElement('span');
		msgElem.innerHTML = 'Вы не заполнили все поля!';
		$('form.form-horizontal').append(msgElem);
		return false;
	}
	return true;
}
