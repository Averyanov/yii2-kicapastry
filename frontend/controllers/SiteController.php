<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Course;
use common\models\Feedback;
use yii\widgets\ActiveForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $courseModel = new Course();
        $mainCourse = $courseModel->find()->where(['id' => 65])->one();
        $subMainCourseDataProvider = new ActiveDataProvider([
            'query' => $courseModel->find()->where(['id' => [72,73]])
        ]);

//        VarDumper::dump($courseModel
//            ->find()
//            ->joinWith('translation')
//            ->where(['not in', 'course.id', [65,72,73]])
//            ->andWhere(['not',['course.owner_id' => null]])
//            ->andWhere(['course.status' => true])
//            ->andWhere(['local_status' => true]),10,1);die;

        $listCoursesDataProvider = new ActiveDataProvider([
            'query' => $courseModel
                ->find()
                ->joinWith('translation')
                ->where(['not in', 'course.id', [65,72,73]])
                ->andWhere(['not',['course.owner_id' => null]])
                ->andWhere(['course.status' => true])
                ->andWhere(['local_status' => true]),
            'pagination' => [
                'pageSize' => 4 // todo move to admin settings
            ]
        ]);

        return $this->render('index',[
            'mainCourse' => $mainCourse,
            'courseModel' => $courseModel,
            'subMainCourseDataProvider' => $subMainCourseDataProvider,
            'listCoursesDataProvider' => $listCoursesDataProvider
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new Feedback();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    // TODO shift to site/actionPage about and policy

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionPolicy()
    {
        return $this->render('policy');
    }
}