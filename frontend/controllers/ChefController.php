<?
namespace frontend\controllers;

use yii\web\Controller;
use common\models\ChefSearch;
use Yii;
use common\models\Chef;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

class ChefController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Chef::find()->where(['status' => true]),
            'pagination' => [
                'pageSize' => false
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($url)
    {
        return $this->render('view', [
            'model' => Chef::find()->where(['url' => $url])->one(),
        ]);
    }

    protected function findModel($params)
    {
        if (($model = Chef::find()->joinWith('translation')->where($params)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}