<?php

namespace frontend\controllers;

use Yii;
use common\models\Course;
use common\models\CourseSearch;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CUrl;
use common\models\User;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'    => ['POST'],
                    'subscribe' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($url)
    {
        $this->layout = 'main';

        $model = Course::find()->where(['url' => $url])->one();

        if(Yii::$app->request->post('Course'))
        {
            if (Yii::$app->user->isGuest) {
                Url::remember('', 'login_before');
                return $this->redirect(CUrl::to(['user/login']));
            }

            // favorite item removed in beforeSave event in Cart model
            Yii::$app
                ->user
                ->identity
                ->link('courses', $model);

            return $this->redirect(CUrl::to(['cart/index']));
        }

        Url::remember();

        return $this->render('view', [
            'model' => $model,
            'owner' => $model->owner
        ]);
    }

    // future courses
    public function actionBroadcast()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Course::find()->where([
                '>',
                'start_date',
                new Expression('NOW()')
            ])
        ]);

        return $this->render('broadcast',[
            'dataProvider' => $dataProvider
        ]);
    }

    // show alredy started && active && contain video order by start_date desc
    public function actionOnline($attr = null, $value = null)
    {
        $model = new Course();
        $dataProvider = $model->getOnlineByCondition($attr, $value);

        return $this->render('searchList',[
            'dataProvider' => $dataProvider,
            'list' => ArrayHelper::getColumn($model->find()->joinWith('translation')->all(), 'name')
        ]);
    }

//    public function actionGetList()
//    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        return ArrayHelper::getColumn(Course::find()->joinWith('translation')->all(), 'name');
//    }

    public function actionSearch()
    {
        $query_str = Yii::$app->request->get('query');

        $dataProvider = new ActiveDataProvider([
            'query' => Course::find()->joinWith('translation')->where(['like', 'name', $query_str])
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->render('filteredList',[
            'dataProvider' => $dataProvider,
            'list' => ArrayHelper::getColumn(Course::find()->joinWith('translation')->all(), 'name')
        ]);
    }

    public function actionFavorite($id)
    {
        // ...и то, чего не может быть, чаще другого случается...
        if(Yii::$app->user->isGuest)
            return $this->redirect(Url::previous());

        // cart item is removed in beforeSave event in Favorite model

        Yii::$app
            ->user
            ->identity
            ->link('favoriteCourses', Course::findOne($id));

        return $this->redirect(Url::previous());
    }

    public function actionSubscribe()
    {
        $user = new User();
        $post = Yii::$app->request->post();

        if($user->findByUseremail($post['email']))
        {
            Yii::$app->session->setFlash(
                'error',
                'User with email'. $post['email'] .' alredy registered. Тимур, треба давати можливість підписки зареєстрованим?'
            );

            return $this->redirect(['site/index']);
        }
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($params)
    {
        if (($model = Course::find()->joinWith('translation')->where($params)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
