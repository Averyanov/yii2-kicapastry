<?
use common\components\CUrl;
?>
<div class="cart_items" style="background-color: #fff;">
    <div class="col-sm-3">
        <div class="cart_img text-center">
            <img src='<?= $model->image ?>'>
        </div>
    </div>
    <div class="col-sm-7">
        <a href='<?= CUrl::to(['course/view', 'url' => $model->url ]) ?>' class="course_title"><?= $model->name ?></a>
        <span class="course_lable">Шеф: </span><a class="course_chif" href='<?= CUrl::to(['chef/view', 'url' => $model->owner->url ]) ?>'><?= $model->owner->name ?></a><br>
        <span class="course_lable">Длительность: </span><span class="course_time"> 2016-11-21</span>
    </div>
    <div class="col-sm-2">
        <span class="course_price"><?= $model->price ?> €</span>
    </div>
</div>
<br>