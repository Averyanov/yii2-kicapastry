<?
use frontend\assets\SearchAsset;

SearchAsset::register($this);

$this->title = Yii::t('app', 'all_online_courses');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="row">
        <?= $this->render('filteredList',
            ['dataProvider' => $dataProvider, 'list' => $list])
        ?>
        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" style="margin-bottom:20px">
            <div>
                <?= $this->render('subscribeForm') ?>
            </div>
            <div class="btnSocial">
                <?= $this->render('/site/socialButtons',[
                    'class' => 'col-sm-6 text-center social-follow-ico'
                ]) ?>
            </div>
        </div>
    </div>
</div>