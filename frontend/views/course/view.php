<?
use common\components\CUrl;
?>

<?
$this->title = $model->meta_title;
?>

<div style="background:url(<?= $owner->image ?>);min-height:500px;background-size:cover;background-position: 50% 60%;background-repeat:no-repeat;">
    <div class="lBlockOver">
        <div class="container">
            <div class="col-md-8 form-about">
                <a href='<?= CUrl::to(['chef/view', 'url' => $owner->url]) ?>' style="color:#fff;display:inline;">
                    <img class="chiefPhoto" src="<?= $owner->getImage() ?>" style="display:inline;">
                    <h2 style="display:inline;margin-left:20px;">
                        <?= $owner->name ?>
                    </h2>
                </a>
                <div class="favorite-add-heart">
                    <?= $model->_isLiked
                        ? "<a class='checked'> ".Yii::t('app', 'added_to_favorites')."</a>"
                        : "<a href='".CUrl::to(['course/favorite', 'id' => $model->id])."'><img src='/image/heart.png'> ".Yii::t('app', 'add_to_favorites')."</a>"
                    ?>
                </div>
                <h2 style="color:#fff;"><?= $model->name ?></h2>
                <div class="lTerm lTerm-buy lead col-xs-12 col-sm-7">

                    <p>
                        <span style="font-size: large;">
                            <?= Yii::t('app', 'duration') ?>: <br><br>1 день -<strong>8 ч 17 м</strong>
                        </span>
                    </p>
                    <p>
                        <span style="font-size: large;">2 день -<strong>7 ч 34 м</strong></span>
                    </p>
                    <p>
                        <span style="font-size: large;"><strong></strong>3 день -<strong>8 ч 45 м</strong></span>
                    </p>
                    <p>
                        <span style="font-size: large;">
                            <?= Yii::t('app', 'cost') ?>: <?= $model->price ?> EUR
                        </span>
                    </p>
                </div>
                <div id="maxShare">
                    <div id="facebook" data-url="http://kicapastry.com/course/Mark_de_Passorio/" data-text="Красный тунец от Шефа Марка Де Пассорио" data-title="Share" class="sharrre"><div class="box"><a class="count" href="#">0</a><a class="share" href="#">Share</a></div></div>

                    <div id="comment" class="sharrre">
                        <div class="box"><a class="count" name="comment_count" href="#comments">48</a><a class="share" href="#comments">COMMENT</a></div>
                    </div>

                    <div id="views" class="sharrre">
                        <div class="box"><a class="count" href="#">2835</a><a class="share" href="#">views</a></div>
                    </div>

                    <div id="shares" class="sharrre">
                        <div class="box"><a class="count" href="#">56</a><a class="share" href="#">shares</a></div>
                    </div>
                </div>
            </div> <!-- about-->
            <div class="col-xs-12 col-md-4 form-subscribe">
<!--                <div class="text-left formBlockGate">-->
<!--                    <h3><a href="#logIn" name="login"> Войти </a> <a class="active" href="#signin" name="fsignin"> Зарегистрироватся</a></h3>-->
<!--                    <div class="fbody">-->
<!--                        <h2 class="text-center" style="color:#2d515b;margin-bottom:20px;">Заполни форму и получи видео-урок на почту!</h2>-->
<!--                        <!--  LOGIN FORM -->-->
<!--                        <form action="/course/Mark_de_Passorio/payment/" method="GET" class="formLand hidden" name="logIn">-->
<!--                            <center><span>Только для зарегестрированных пользователей</span></center>-->
<!--                            <div class="form-group">-->
<!--                                <label for="iEmail" class="sr-only">Email</label>-->
<!--                                <input type="email" class="form-control input-lg" id="iEmail" name="email" placeholder="Email" value="" required="">-->
<!--                            </div>-->
<!--                            <div class="form-group" id="require_pass">-->
<!--                                <label for="iPass" class="sr-only">Пароль</label>-->
<!--                                <input type="password" class="form-control input-lg" id="iPass" name="pasw" placeholder="Пароль">-->
<!--                            </div>-->
<!---->
<!--                            <div class="form-group">-->
<!---->
<!---->
<!--                                <input type="hidden" name="courseType" id="courseOline" value="online">-->
<!--                                <input type="hidden" name="course_price" id="course_price" value="0">-->
<!--                                <input type="hidden" name="course_offline_price" id="course_offline_price" value="0">-->
<!--                            </div>-->
<!--                            <center>-->
<!--                                <button type="submit" class="btn btn-primary btn-lg" style="font-size:18pt;">Отправить</button>-->
<!--                            </center>-->
<!--                            <div class="form-group">-->
<!--                                <center>-->
<!--                                    <a name="goto" href="/users/forgot/" class="flr">Забыли пароль?</a>-->
<!--                                </center>-->
<!--                            </div>-->
<!--                            <input type="hidden" value="Mark_de_Passorio" name="course">-->
<!--                        </form>-->
<!--                        <!-- SIGN IN FORM  -->-->
<!---->
<!--                        <form action="/course/Mark_de_Passorio/payment/" id="ajax_test" class="formLand" name="fsignIn" onsubmit="handleOutboundLinkClicks();return true;">-->
<!---->
<!--                            <div class="form-group">-->
<!---->
<!--                                <label for="iName" class="sr-only">Ваше имя</label>-->
<!--                                <input type="text" class="form-control input-lg" id="fiName" name="name" placeholder="Имя" value="" required="true" autocomplete="off">-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="iTel" class="sr-only">Телефон</label>-->
<!--                                <input type="text" class="form-control input-lg" id="fiTel" name="tel" placeholder="Телефон" value="" autocomplete="off">-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="iEmail" class="sr-only">Email</label>-->
<!--                                <input type="email" class="form-control input-lg" id="fiEmail" name="email" placeholder="Email" value="" autocomplete="off">-->
<!--                            </div>-->
<!---->
<!--                            <div class="form-group">-->
<!---->
<!---->
<!--                                <input type="hidden" name="courseType" id="courseOline" value="online">-->
<!--                                <input type="hidden" name="course_price" id="course_price" value="0">-->
<!--                                <input type="hidden" name="course_offline_price" id="course_offline_price" value="0">-->
<!--                            </div>-->
<!---->
<!--                            <div class="form-group hide">-->
<!--                                <label for="agreement" class="agreement">-->
<!--                                    <input type="checkbox" id="agreement" name="agreement" checked="checked"> я согласен с лицензионным соглашением-->
<!--                                </label>-->
<!--                            </div>-->
<!--                            <div class="warning hidden">-->
<!--                                <center>-->
<!--                                    <span class="warning"> Заполните все поля в форме </span>-->
<!--                                </center>-->
<!--                            </div>-->
<!--                            <center>-->
<!---->
<!--                                <span onclick="checkFirstForm();" class="btn btn-primary btn-lg" style="font-size:18pt;" id="fakebutton">Отправить</span>-->
<!---->
<!--                            </center>-->
<!---->
<!--                            <input type="hidden" value="Mark_de_Passorio" name="course">-->
<!---->
<!--                        </form>-->
<!--                        <div class="shtora"><h4>Вы ввели :</h4>-->
<!--                            <br>-->
<!--                            <p></p>-->
<!---->
<!--                            <span>Вам автоматически будет <b>сгенерирован</b> и отправлен <b>пароль на</b> указанную вами <b>почту</b></span><br>-->
<!--                            <span class="hide"><b>Вы согласились </b><a href="#">лицензионным соглашением</a></span>-->
<!--                            <br>-->
<!--                            <span><b>Ваши</b> личные <b>данные не будут переданы</b> третьим лицам</span>-->
<!--                            <!-- -->-->
<!--                            <form action="/course/Mark_de_Passorio/payment/" method="GET" name="mirrow" class="formConformSignin">-->
<!---->
<!--                                <input type="hidden" name="name">-->
<!--                                <input type="hidden" name="tel" value="+380">-->
<!--                                <input type="hidden" name="email">-->
<!--                                <input type="hidden" name="agreement" checked="checked" value="on">-->
<!--                                <div class="form-group">-->
<!--                                    <input type="hidden" name="courseType" id="courseOline" value="online">-->
<!--                                    <input type="hidden" name="course_price" id="course_price" value="0">-->
<!--                                    <input type="hidden" name="course_offline_price" id="course_offline_price" value="0">-->
<!--                                </div>-->
<!--                                <center>-->
<!--                                    <span onclick="backFirstForm();" class="btn btn-primary btn-lg" style="font-size:18pt;"><span class="glyphicon glyphicon-pencil"></span>ред.</span>-->
<!--                                    <button class="btn btn-primary btn-lg" style="font-size:18pt;" onclick="ga('send', 'event', 'FreeMasterClass', 'submit', getAvaliableH1());">Отправить</button>-->
<!--                                </center>-->
<!---->
<!--                                <input type="hidden" value="Mark_de_Passorio" name="course">-->
<!--                            </form>-->
<!--                            <!-- -->-->
<!--                        </div>-->
<!--                        <!-- end FORM  -->-->
<!---->
<!--                    </div>-->
<!--                </div>-->
            </div>



        </div>
    </div> <!--cover-->
</div>
<div style="background:url();background-size:cover;">
    <div style="background:rgba(255,255,255,0.9);height:100%;width:100%;padding:30px 0;">
        <div class="container">


            <div class="col-sm-12 l2Program">
                <h1>
                    <?= Yii::t('app', 'program') ?>
                </h1>
                <ul>
                    <?= $model->content ?>
                </ul>
            </div>





        </div>
    </div> <!--cover-->
</div>
<div style="background-image:url(<?= $owner->image ?>);min-height:500px;background-size:cover;/*background-position: 50% 50%;*/background-repeat:no-repeat;">



    <div class="container">

        <div class="col-md-5">
            <div class="text-left" style="margin-top:20px;background:#faf9e6;width:100%;">

                <div style="padding:10px 30px;">
                    <h2 class="text-center" style="color:#c2b409;margin-bottom:20px;">Заполни форму и получи видео-урок на почту!</h2>
                    <br>
                    <?=
                    $this->render(
                            '/course/toCartForm', [
                                'model' => $model,
                                'showPrice' => false,
                                'glyphiconSize' => 20
                            ])
                    ?>
                    <br>
                </div>
                <br>
            </div>
            <br>
            <br>
        </div>

        <div class="col-md-7">
            &nbsp;
            <br>
            <br>
        </div>


    </div>
</div>
<div id="comment" style="background:#fff;padding:30px 0;">


    <div class="container">
        <h3 id="comments">
            <?= Yii::t('app', 'comments') ?>
        </h3>

    </div>


</div>