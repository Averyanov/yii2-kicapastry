<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="col-lg-9 col-md-8 col-sm-7 nearest_broadcast">
    <div class="col-sm-12">


        <div class="list_title" style="padding: 20px 0px 10px 0px;">
            <h3 style="display: inline">Ближайшая трансляция</h3>
            <a href="/broadcast/" class="index-all-link">Все трансляции</a>
        </div>

        <div class="col-lg-8 col-md-7 col-xs-12 nearest-broadcast-img" style="background-image: url();">
            <div class="time_left">
                <span>до начала</span>
                <span>1</span>
                <span>день</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 col-xs-12 nearest_info">
            <a href="" class="course_title"></a>
            <div class="social_ico hidden-sm hidden-xs">
                <span class="herth">25</span>
                <span class="comments">4</span>
                <span class="views">565</span>
            </div>
            <span class="course_lable hidden-sm hidden-xs">Шеф:</span><a class="course_chif hidden-sm hidden-xs" href="#">Татьяна Вербицкая</a><br class="hidden-sm hidden-xs">
            <span class="course_lable hidden-sm hidden-xs">2-3 апреля 2016</span><br class="hidden-sm hidden-xs">
            <span class="course_lable">Стоимость: </span><span class="online_price">20$</span>
<!--            <center><a href="#" class="btn toCart-nearest btn-primary">Получить доступ</a></center>-->
<!--            tmp buy form-->
            <? $form = ActiveForm::begin([
                'action' => "/course/6_name_ru"
            ]); ?>
            <center>
                <?= Html::submitButton(
                    Html::tag('span', null, ['class' => 'glyphicon glyphicon-shopping-cart', 'style' => 'font-size: 14pt;']).'Получить доступ',
                    [
                        'class' => 'btn btn-primary btn-lg',
                        'style' => 'font-size: 14pt; position: relative; top: 100px;',
                        'name'  => 'Course',
                        'value' => 234,
                    ]
                ) ?>
            </center>
            <? ActiveForm::end(); ?>

        </div>
    </div>
</div>