<?
use yii\widgets\ListView;

$this->title = 'Будущие Мастер-Классы';
$this->params['breadcrumbs'][] = 'Ближайшие Мастер-Классы';
?>
<div class="col-md-9">
    <?= $this->render('broadcastDesc') ?>
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'tag' => null
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-md-6 col-sm-6 col-xs-12 sCol'
        ],
        'itemView' => 'listItem',
        'emptyTextOptions' => ['tag' => 'h3'],
        'emptyText' => 'На ближайшее время мероприятий не запланировано.'
    ])
    ?>
</div>
<div class="col-md-3 hidden-xs hidden-sm">
    <div>
        <?= $this->render('/course/subscribeForm') ?>
    </div>
    <div class="btnSocial">
        <?= $this->render('/site/socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
    </div>
</div>