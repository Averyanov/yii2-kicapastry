<div class="sBlock" style="background:url(<?= $model->image ?>); background-size: cover;">
    <div class="sLabel">
        <span id="courseOlinePrice"><?= $model->price ?></span>€
    </div>
    <a href="/course/<?= $model->url ?>" class="sCover">
        <?= $model->name ?>
        <div class="sCoverLine">
            <div class="text-left col-xs-4">
                <span class="glyphicon glyphicon-heart"></span> 86
            </div>
            <div class="text-right col-xs-8" style="padding-right:20px;">
                <span class="glyphicon glyphicon-comment"></span> 0 &nbsp;&nbsp;
                <span class="glyphicon glyphicon-eye-open"></span> 2807
            </div>
        </div>
    </a>

</div>