<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\components\CUrl;

?>
<div class="subscribe" style="background-image: url(/image/b1.jpg)">
    <p>
        <?= Yii::t('app', 'subscribe_form_caption'); ?>
    </p>
    <p class="text-center">
        <b style="text-transform:uppercase;">
        </b>
    </p>
    <? $form = ActiveForm::begin([
        'action' => CUrl::to(['course/subscribe']),
        'method' => 'post'
    ]); ?>
    <div class="form-group">
        <?= Html::textInput(
                'name',
                false,
                [
                    'placeholder' => Yii::t('app', 'name'),
                    'class' => 'form-control',
                    'type' => 'text',
                    'required' => true,
                    'pattern' => '[A-Za-zА-Яа-яЁё]+'
            ]) ?>
    </div>
    <div class="form-group">
        <?= Html::textInput(
                'email',
                false,
                [
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                    'type' => 'email',
                    'required'=>true
                ]
        ) ?>
    </div>
    <center>
        <?= Html::submitButton(
                Yii::t('app', 'subscribe_form_button'),
                [
                    'name' => 'doSubscribe',
                    'class' => 'btn btn-primary',
//                    'disabled' => true
                ]) ?>
    </center>
    <? ActiveForm::end(); ?>
<!--    <form method="POST" class="subscribe-form" onsubmit="handleOutboundLinkClicksTwo();return true;">-->
<!--        <input type="hidden" value="free_tuil" name="campaign">-->
<!--        <input type="hidden" value="tuil" name="course">-->
<!--        <div class="form-group">-->
<!--            <input type="text" class="form-control" placeholder="Имя" value="" name="name">-->
<!--        </div>-->
<!--        <div class="form-group">-->
<!--            <input type="email" class="form-control" placeholder="Email" value="" name="email">-->
<!--        </div>-->
<!--        <center>-->
<!--            <button type="submit" name="doSubscribe" value="1" class="btn btn-primary">Получить бесплатный доступ</button>-->
<!--        </center>-->
<!--    </form>-->
</div>