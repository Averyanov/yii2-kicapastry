<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\components\CUrl;
?>
<div class="col-xs-3">
    <div class="cart_img text-center">
        <img src="<?= $model->image ?>">
    </div>
</div>
<div class="col-xs-7">
    <a href='<?= CUrl::to(['favorite/delete', 'id' => $model->id]) ?>' class="course_del">Удалить</a>
    <? $form = ActiveForm::begin(['action' => CUrl::to(['course/view', 'url' => $model->url])]); ?>
        <?= Html::submitButton('Добавить в корзину', [
                'class' => 'course_favorite',
                'name'  => 'Course',
                'value' => $model->price
        ]) ?>
    <? ActiveForm::end(); ?>
    <a href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>' class="course_title">
        <?= $model->name ?>
    </a>
    <span class="course_lable">Шеф: </span>
    <a class="course_chif" href='<?= CUrl::to(['chef/view', 'url' => $model->owner->url]) ?>'>
        <?= $model->owner->name ?>
    </a><br>
    <?= $model->start_date ?>
</div>
<div class="col-xs-2">
    <span class="course_price"><?= $model->price ?> €</span>
</div>