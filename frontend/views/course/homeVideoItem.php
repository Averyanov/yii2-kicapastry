<?
use common\components\CUrl;
use yii\helpers\Html;
?>
<div class='onlineBlock' style='background-image: url(<?= $model->getImage() ?>)'>
    <a class='sCover' href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>'>
        <div class='sIcoPlay' style='bottom: 65%;'>
            <div>
                <span class='glyphicon glyphicon-play'></span>
            </div>
        </div>
    </a>
</div>
<div class='onlineInfo'>
    <a class='video_name' href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>'>
        <?= $model->name ?>
    </a>
    <br>


    <?
    $owner = $model->owner ?: null;
    ?>

    <?=
    $owner ? Yii::t('app', 'chef').': ' : ''
    ?>

    <?=
    $owner
        ? Html::a(
            $owner->name,
            CUrl::to(['chef/view', 'url' => $model->owner->url]),
            ['class' => 'chif_lnk']
        )
        : ''
    ?>


    <br>
    <?=
    $this->render(
        $model->price ? '/course/toCartForm' : '/course/priceOnly', [
            'model' => $model,
            'showPrice' => true,
            'glyphiconSize' => 14
    ])
    ?>
</div>