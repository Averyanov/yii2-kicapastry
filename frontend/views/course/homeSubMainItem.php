<?
use common\components\CUrl;
?>
<div class='sBlock sub-main-item' style='background-image:url(<?= $model->image ?>);'>
    <div class='sLabel'>
        <span id='courseOlinePrice'><?= $model->price ?></span>€
    </div>
    <div class='sCover'>
        <a href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>'>
            <?= $model->name ?>
        </a>
        <div class='sChief hidden-xs'>
            <a href='<?= CUrl::to(['chef/view', 'url' => $model->owner->url]) ?>'>
                <img class='chiefPhotoSm' src='<?= $model->owner->getImage() ?>'>
            </a>
        </div>
        <div class='sCoverLabel'>
            <?= Yii::$app->formatter->asDatetime($model->end_date, "php:d.m.Y") ?>
        </div>
<!--        <div class='sCoverLabel2'>-->
<!--            Осталось: 33 мест-->
<!--        </div>-->
        <div class='sCoverLine'>
            <div class='text-right col-xs-8 pull-right padding-right20'>
                <span class='glyphicon glyphicon-comment'></span> 0 &nbsp;&nbsp;
                <span class='glyphicon glyphicon-eye-open'></span> 10262
            </div>
        </div>
    </div>

</div>