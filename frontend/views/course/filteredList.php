<?
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
?>

<div class="col-lg-9 col-md-8 col-sm-7">
    <? Pjax::begin([
        'enablePushState' => false,
        'options' => [
            'class' => 'search-form-wrap',
            'id' => 'search_form_wrap'
        ]
    ]) ?>
    <?

    $this->registerJs(
        '$(".typeahead").typeahead({
            source : '. json_encode($list) .',
            autoSelect: false,
            items: 10,
            item: "<li><a href=\'#\' role=\'option\'></a></li>",
            afterSelect: function (e) {
                var form = $(this.$element).closest("form");
                form.submit();
            },
            minLength: 0
        });'
    );
    ?>
    <form action="/course/search" method="get" data-pjax="" class="form-horizontal">
        <div class="form-group">
            <div class="col-md-10">
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <?=
                    Html::input(
                        'search',
                        'query',
                        null,
                        [
                            'id' => 'searchVideo',
                            'data-provide' => 'typeahead',
                            'class' => 'typeahead form-control btn-md',
                            'autocomplete' => 'off',
                            'placeholder' => 'Поиск онлайн мастер-класса',
                        ]
                    )
                    ?>
                </div>
            </div>
        </div>
    </form>
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'class' => 'row',
            'style' => 'margin-bottom: 20px'
        ],
        'itemOptions' => [
            'class' => 'col-lg-4 col-md-6 col-xs-12 sCol'
        ],
        'itemView' => 'homeVideoItem'
    ])
    ?>
    <? Pjax::end()?>
</div>