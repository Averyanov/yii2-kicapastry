<?
use common\components\CUrl;
use yii\helpers\Html;
?>
<a href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>'>
    <img src="<?= $model->image ?>" class="pull-left cart-item-img">
</a>
<div class="sDate"><?= Yii::$app->formatter->asDate($model->end_date, 'dd.MM.yyyy'); ?></div>
<a href='<?= CUrl::to(['course/view', 'url' => $model->url]) ?>' class="sTitle"><?= $model->name ?></a>
<br>
<?
$owner = $model->owner ?: null;
?>

<?=
$owner ? 'Шеф:' : ''
?>

<?=
$owner
    ? Html::a(
        $owner->name,
        CUrl::to(['chef/view', 'url' => $owner->url]),
        ['class' => 'paymentInfo']
    )
    : ''
?>
<br>
Вид участия: Онлайн
<br>
<br>
<div class="clearfix"></div>
<br>