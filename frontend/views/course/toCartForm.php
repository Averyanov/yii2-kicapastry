<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\components\CUrl;
use common\models\User;
?>
<?
    if($showPrice)
        echo Yii::t('app', 'cost').": <span class='online_price'><span id='courseOlinePrice'> $model->price </span>€</span>"
?>
<!--<div class="clearfix"></div>-->
<? $form = ActiveForm::begin([
        'action' => CUrl::to(['course/view', 'url' => $model->url])
]); ?>
    <center>
        <?= Html::submitButton(
            Html::tag('span', null, ['class' => 'glyphicon glyphicon-shopping-cart', 'style' => 'font-size: '. $glyphiconSize .'pt;']).' '.Yii::t('app', 'buy_now'),
            [
                'class' => 'btn btn-primary btn-lg',
                'style' => 'font-size: '. $glyphiconSize .'pt;',
                'name'  => 'Course',
                'value' => $model->price,
                'disabled' => User::has($model->id)
            ]
        ) ?>
    </center>
<? ActiveForm::end(); ?>