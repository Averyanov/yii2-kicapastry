<?
use common\components\CUrl;
use yii\helpers\Html;
?>
<div class="cart_items">
    <div class="col-xs-3">
        <div class="cart_img text-center">
            <img src="<?= $model->image ?>">
        </div>
    </div>
    <div class="col-xs-6">
        <a href='<?= CUrl::to(['cart/delete', 'id' => $model->id ]) ?>' class="course_del">Удалить</a>
        <a href='<?= CUrl::to(['course/favorite', 'id' => $model->id ]) ?>' class='course_favorite'>Добавить в избранное</a>
        <a href='<?= CUrl::to(['course/view', 'url' => $model->url ]) ?>' class="course_title"><?= $model->name ?></a>
        <span class="course_lable">Шеф: </span>
        <a class="course_chif" href='<?= CUrl::to(['chef/view', 'url' => $model->owner->url]) ?>'><?= $model->owner->name ?></a>
        <br>
        <?= $model->start_date ?>
    </div>
    <div class="col-xs-3">
        <span class="course_price"><?= Yii::$app->formatter->asDecimal($model->price - $model->price * $discount / 100) ?> €</span>
        <?
        if($discount)
        {
            echo Html::tag('span', Yii::t('app', 'saving'), ['class' => 'course_economy']);
            echo Html::tag('span', Yii::$app->formatter->asDecimal($model->price * $discount / 100).' €', ['class' => 'course_economy_summ']);
        }
        ?>
    </div>
</div>
<br>