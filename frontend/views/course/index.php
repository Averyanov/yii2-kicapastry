<?
use yii\widgets\ListView;

$this->title = 'Онлайн мастер класс';
$this->params['breadcrumbs'][] = 'Мастер класс - просмотп (ленд)';
?>
<div class="col-md-9">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'tag' => null
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-md-6 col-sm-6 col-xs-12 sCol'
        ],
        'itemView' => 'listItem'
    ])
    ?>
</div>
<div class="col-md-3 hidden-xs hidden-sm">
    <div>
        <?= $this->render('/course/subscribeForm') ?>
    </div>
    <div class="btnSocial">
        <?= $this->render('/site/socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
    </div>
</div>