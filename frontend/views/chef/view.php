<?
use yii\widgets\ListView;
use yii\db\Expression;
use common\components\CUrl;
?>
<?
$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = ['label' => 'Шефы', 'url' => CUrl::to(['chef/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<!--    chief-block [begin]-->
    <div class="chief-block">
        <div class="col-md-9 chief-description">
            <h1 class="cheifTitle"><?= $model->name ?></h1>

            <div class="chiefs-details hidden-xs">
                <?= $model->rank ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="chiefs-social-top">
            <div class="chiefs-social">
                <div id="maxShare">
                    <div id="facebook" data-url="http://kicapastry.com/chiefs/Christophe_Renou/" data-text="Кристоф Рену" data-title="Share" class="sharrre"><div class="box"><a class="count" href="#">0</a><a class="share" href="#">Share</a></div></div>
                    <div id="comment" class="sharrre">
                        <div class="box"><a class="count" name="comment_count" href="#comments">0</a><a class="share" href="#comments">COMMENT</a></div>
                    </div>

                    <div id="views" class="sharrre">
                        <div class="box"><a class="count" href="#">2912</a><a class="share" href="#">views</a></div>
                    </div>

                    <div id="shares" class="sharrre">
                        <div class="box"><a class="count" href="#">0</a><a class="share" href="#">shares</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 hidden-sm hidden-xs">
            <img class="chiefPhotoBig" src="<?= $model->getImage('full') ?>">
        </div>
    </div>
<!--    chief-block [end]-->
</div>
<div class="chiefGallery"></div>
<!--video courses container [begin]-->
<div class="container">
    <div class="row">
        <?= $this->render('/course/closestList') ?>
        <div class="col-lg-3 col-md-4 col-sm-5" style="margin-top:20px;">
            <div>
            <?= $this->render('/course/subscribeForm')?>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="list_title">
                <h3 class="caption">Записи онлайн Мастер-Классов</h3>
                <a href="/course/online" class="index-all-link">Все записи</a>
            </div>
            <div class="row">
                <?=
                ListView::widget([
                    'dataProvider' => $model->getCourseDataProvider(['price' => 0]),
                    'layout' => '{items}',
                    'options' => [
                        'tag' => false
                    ],
                    'itemOptions' => [
                        'tag' => 'div',
                        'class' => 'col-lg-4 col-md-4 col-sm-6 sCol'
                    ],
                    'itemView' => '/course/chefItem'
                ])
                ?>
<!--                <h3 class="col-sm-12" style="margin: 10px 0px 15px 0px;">Прошедшие мастер классы:</h3>-->
<!--                --><?//=
//                ListView::widget([
//                    'dataProvider' => $model->getCourseDataProvider([
//                        '<',
//                        'end_date',
//                        new Expression('NOW()')
//                    ]),
//                    'layout' => '{items}',
//                    'options' => [
//                        'tag' => false
//                    ],
//                    'itemOptions' => [
//                        'tag' => 'div',
//                        'class' => 'col-sm-6 col-xs-12'
//                    ],
//                    'itemView' => '/course/pastItem'
//                ])
//                ?>
            </div>  <!--  end of row  -->

        </div> <!--col-->
        <div class="col-sm-2">
            <div class="btnSocial-chiefs">
                <?= $this->render(
                    '/site/socialButtons',
                    ['class' => 'chiefs-follow'])
                ?>
            </div>
        </div>
    </div>
</div>
<!--video courses container [end]-->