<?
use yii\widgets\ListView;

$this->title = Yii::t('app', 'chefs');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="container">
    <div class="col-lg-9 col-md-8 col-sm-7">
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => [
                'class' => 'chiefsList'
            ],
            'itemOptions' => [
                'class' => 'col-md-3 col-xs-6'
            ],
            'itemView' => 'homeItem'
        ])
        ?>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" style="margin-bottom:20px">
        <div>
            <?=
            $this->render('/course/subscribeForm')
            ?>
        </div>
        <div class="btnSocial">
            <?= $this->render(
                '/site/socialButtons',
                ['class' => 'text-center social-follow-ico'])
            ?>
        </div>
    </div>
</div>