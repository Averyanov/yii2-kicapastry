<?
use yii\widgets\ListView;

$this->title = 'Избранное';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-9">
    <span class="list_title">
        <?= $dataProvider->getCount()
            ? Yii::t('app',
                'В корзине {n, plural, one{# мастер-класс} =2{# мастер-класса} =3{# мастер-класса} =4{# мастер-класса} =21{# мастер-класса} other{# мастер-классов}}',
                ['n' => $dataProvider->getCount()])
            : false
        ?>
    </span>
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => ['tag' => null],
        'itemOptions' => ['class' => 'cart_items'],
        'itemView' => '/course/favoriteItem',
        'emptyText' => 'В избранном пока что нет мастер-классов',
        'emptyTextOptions' => ['tag' => 'h3']
    ])
    ?>
</div>