<?
return;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\helpers\Html;
?>

<?
// TODO move to model
$socials = array_filter([
    [
        'name' => 'fb',
        'url' => 'https://www.facebook.com/KievInternationalCulinaryAcademy/',
        'img' => '/image/social-fb-big.png',
        'languages' => ['ru','en','es']
    ],
    [
        'name' => 'vk',
        'url' => 'https://vk.com/kicaukraine',
        'img' => '/image/social-vk-big.png',
        'languages' => ['en','es']
    ],
], function($item) {
    $langage = substr(Yii::$app->language, 0, -3);
    return in_array($langage, $item['languages']);
});
?>

<?
$dataProvider = new ArrayDataProvider([ 'allModels' => $socials ])
?>

<h3 style="text-align: center;">
    <?=
    $dataProvider->getCount()
        ? Yii::t(
            'app',
            'social_buttons_caption',
            ['n' => $dataProvider->getCount()]
        )
        : false;
    ?>
</h3>

<?
$n = (12 / ($dataProvider->getCount()?:1))
?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => ['class' => 'row'],
    'itemOptions' => [
        'class' => "col-sm-$n"
    ],
    'showOnEmpty' => true,
    'itemView' => function($item)
    {
        return Html::a(Html::img($item['img']),$item['url']);
    }
]);
?>
