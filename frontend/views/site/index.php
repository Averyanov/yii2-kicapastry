<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
use common\models\Chef;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\components\CUrl;

use frontend\assets\IndexAsset;

IndexAsset::register($this);

$this->title = 'Кондитерские мастер-классы: Онлайн видео мастер классов для кондитеров от Киевской Международной Кулинарной Академии';
?>
<!--main-course-wrapper [begin]-->
<div class="main-course-wrapper">
<!--main-course-inner [begin]-->
    <div class="main-course-inner">
        <div class="container">
            <div class="col-md-6">
<!--            main-course-block [begin]-->
                <div class="sBlock main-course-block" style="background-image:url(<?= $mainCourse->image ?>);">
                    <div class="sLabel">
                        <span id="courseOlinePrice"><?= $mainCourse->price ?></span>€
                    </div>
                    <div class="sCover">
                        <a href='<?= CUrl::to(['course/view', 'url' => $mainCourse->url]) ?>'>
                            <h1><?= $mainCourse->name ?></h1>
                        </a>
                        <div class="sDesc hidden-xs">
                        </div>
                        <div class="sChief hidden-xs">
                            <a href='<?= CUrl::to(['chef/view', 'url' => $mainCourse->owner->url]) ?>'>
                                <img class="chiefPhotoSm" src="<?= $mainCourse->owner->getImage() ?>">
                            </a>
                        </div>
                        <div class="sCoverLabel">
                            <?/*=
                            ($mainCourse->end_date < date('Y-m-d'))
                                ? 'Прошел'
                                : ''
                            */?>
                            <?= Yii::$app->formatter->asDatetime($mainCourse->end_date, "php:d.m.Y") ?>
                            <?//= date('Y.m.d'$mainCourse->end_date)->format ?>
                        </div>
                        <div class="sCoverLine">
                            <div class="text-right col-xs-8 pull-right padding-right20">
                                <span class="glyphicon glyphicon-comment"></span> 337 &nbsp;&nbsp;
                                <span class="glyphicon glyphicon-eye-open"></span> 20160
                            </div>
                        </div>
                    </div>
                </div>
<!--            main-course-block [begin]-->
            </div>


            <div class="col-md-6 hidden-xs hidden-sm">

                <?=
                ListView::widget([
                    'dataProvider' => $subMainCourseDataProvider,
                    'layout' => '{items}',
                    'options' => [
                        'tag' => null
                    ],
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'itemView' => '/course/homeSubMainItem'
                ])
                ?>

            </div>



        </div>
    </div>
    <!--main-course-inner [end]-->
</div>
<!--main-course-wrapper [end]-->
<!--homeVideoBlock [begin]-->
<div class="homeVideoBlock">
    <div class="container">
        <div class="col-md-12">
            <div class="list_title">
                <h3 class="caption">
                    <?= Yii::t('app', 'online_courses') ?>
                </h3>
                <a href='<?= CUrl::to(['course/online']) ?>' class="index-all-link">
                    <?= Yii::t('app', 'all_online_courses') ?>
                </a>
            </div>

            <?=
            ListView::widget([
                'dataProvider' => $listCoursesDataProvider,
                'layout' => '{items}',
                'options' => [
                    'tag' => 'div',
                    'class' => 'row'
                ],
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'col-lg-3 col-md-4 col-sm-6 sCol home-video-item',
                ],
                'itemView' => '/course/homeVideoItem'
            ])
            ?>
        </div>
    </div>
</div>
<!--homeVideoBlock [end]-->
<div class="homeChiefBlock">
    <div class="container">
        <div class="col-md-12">
            <div class="list_title">
                <h3 class="caption">
                    <?= Yii::t('app', 'chefs_of_culinary_academy') ?>
                </h3>
                <a href='<?= CUrl::to(['chef/index']) ?>' class="index-all-link"><?= Yii::t('app', 'all_chefs') ?></a>
            </div>
            <div class="chiefsList">
                <?=
                ListView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => Chef::find()
                            ->joinWith('translation')
                            ->orderBy('name'),
                        'pagination' => [
                            'pageSize' => 6
                        ],
                    ]),
                    'layout' => '{items}',
                    'options' => [
                        'tag' => null
                    ],
                    'itemOptions' => [
                        'tag' => 'span',
                        'class' => 'col-md-2 col-sm-3 col-xs-6',
                    ],
                    'itemView' => '/chef/homeItem'
                ])
                ?>
            </div>
        </div>
    </div>
</div>