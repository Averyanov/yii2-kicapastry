<?

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\SearchAsset;
use common\models\City;

SearchAsset::register($this);

$this->registerJs(
    '$(".typeahead").typeahead({
            source : '. json_encode(City::getCitiesList()) .',
            autoSelect: false,
            items: 10,
            minLength: 0
        });'
);

?>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'phone', ['enableAjaxValidation' => true]) ?>

        <?= $form->field($model, 'city')->textInput([
            'class' => 'typeahead form-control',
            'autocomplete' => 'off'
        ]) ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'mess')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-1"></div>
</div>
