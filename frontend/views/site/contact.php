<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\Feedback */

use yii\bootstrap\Modal;

$this->title = Yii::t('app', 'feedback');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-9 col-md-8 col-sm-7">
        <h3>
            <?= Yii::t('app', 'kiev_international_culinary_academy') ?>
        </h3>
        <p>&nbsp;</p>
        <div class="contact_lable">
            <?= Yii::t('app', 'phone_number') ?>:&nbsp;
            <span>+380 (94) 853 30 54</span></div>
        <div class="contact_text">
            E-mail:
            <?= Yii::$app->params['supportEmail'] ?>
        </div>
        <div class="contact_text">
            <?= Yii::t('app', 'address') ?>:&nbsp;
            <?= Yii::t('app', 'address_value') ?>
        </div>
        <br>
        <br>
        <?
        Modal::begin([
            'header' => '<h3 class="titlecatalog">Заполните, пожалуйста, форму и мы свяжемся с Вами в течении 24 часов.</h3>',
            'class' => 'form-horizontal',
            'toggleButton' => [
                'label' => Yii::t('app', 'send_request'),
                'class' => 'btn btn-primary'
            ],
        ]);
        ?>
        <?= $this->render('contactForm', ['model' => $model]) ?>
        <? Modal::end();?>
    </div>
    <br>
    <div class="col-lg-3 col-md-4 col-sm-5" style="margin-bottom:20px;">
        <div>
            <?= $this->render('/course/subscribeForm') ?>
        </div>
        <div class="btnSocial">
            <?= $this->render('socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
        </div>
    </div>
</div>