<?
use common\components\CUrl;
?>
<a href='<?= CUrl::to(['gallery/view', 'id' => $model->id]) ?>'>
    <div class="recipeBlock" style="background-image:url(<?= $model->image ?>);background-size: cover; background-position: 50% 50%;">
        <div class="recipeCover col-md-5">
            <div class="recipeTitle">
                <?= $model->name ?>
            </div>
        </div>
    </div>
</a>