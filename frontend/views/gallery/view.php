<?
use yii\widgets\ListView;
use yii\helpers\Html;
use frontend\assets\GalleryAsset;
use common\components\CUrl;

GalleryAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Рецепты',
    'url'   => CUrl::to(['gallery/index'])
];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="row">
    <div class="col-lg-9 col-md-8 col-sm-7">
        <div class="opisanie_photo">
            <h3><?= $model->name ?></h3>
            <span><?= $model->date ?></span>
            <p><?= $model->description ?></p>
        </div>

        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => [
                'tag' => 'div',
                'class' => 'row'
            ],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'slide_block col-md-3 col-sm-4 col-xs-6'
            ],
            'itemView' => function($item)
            {
                return Html::a(
                    null,
                    $item->image,
                    [
                        'rel' => 'galleryBox',
                        'style' => "background-image: url($item->image);margin-bottom:10px;display: block; height: 320px;background-size: auto 100%;background-repeat: no-repeat;background-position: 50%;"
                    ]
                );
            }
        ])
        ?>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="col-lg-3 col-md-4 col-sm-5" style="margin-bottom:20px;">
        <div>
            <?= $this->render('/course/subscribeForm') ?>
        </div>
        <div class="btnSocial">
            <?= $this->render('/site/socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
        </div>
    </div>

</div>