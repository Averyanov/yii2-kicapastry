<?
use yii\widgets\ListView;

$this->title = 'Фотогалерея кондитерских мастер-классов, курсы кондитера в Киеве';
$this->params['breadcrumbs'][] = 'Фотогалерея';
?>
<div class="row">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'class' => 'row'
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-sm-4'
        ],
        'itemView' => 'listItem'
    ])
    ?>
</div>
