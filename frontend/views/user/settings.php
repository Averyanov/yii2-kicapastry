<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Анкета пользователя';
$this->params['breadcrumbs'][] = [
    'label' => 'Личный кабинет',
    'url'   => '/user'
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Edit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>