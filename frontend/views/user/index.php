<?
use frontend\assets\CartAsset;
use yii\widgets\ListView;

CartAsset::register($this);

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-9">
    <h3>Ваши онлайн мастер-классы</h3>

    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'tag' => 'div',
            'class' => 'col-sm-10 col-sm-offset-1 cart-list'
        ],
        'itemOptions' => [
            'tag' => false
        ],
        'itemView' => '/course/cabinetItem'
    ])
    ?>

    <div class="clearfix"></div>

</div>