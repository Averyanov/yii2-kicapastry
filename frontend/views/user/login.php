<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\CUrl;

$this->title = Yii::t('app', 'auth');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'account'),
    'url'   => '/user'
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'login_form_desc') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <? $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(Yii::t('app', 'username')) ?>

                <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('app', 'password')) ?>

                <?= $form->field($model, 'rememberMe')->checkbox()->label(Yii::t('app', 'remember_me')) ?>

                <div style="color:#999;margin:1em 0">

                </div>

                <div class="form-group">
                    <?= Html::submitButton(strip_tags(Yii::t('app', 'login')), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <?= Html::a(Yii::t('app', 'signup'), CUrl::to(['user/signup']), ['class' => 'btn btn-success ']) ?>
                    <?= Html::a(Yii::t('app', 'forgot_password'),CUrl::to(['user/request-password-reset']), ['class' => 'btn btn-danger']) ?>
                </div>

            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>