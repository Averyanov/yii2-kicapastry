<?
use yii\widgets\Breadcrumbs;
use common\components\CUrl;
use frontend\assets\GalleryAsset;

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Рецепты',
    'url'   => CUrl::to(['recipe/index'])
];
$this->params['breadcrumbs'][] = $model->name;

GalleryAsset::register($this);
?>
<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => [
            'label' => 'Международная Кулинарная Академия',
            'url'   => CUrl::to(['site/index'])
        ]
    ]) ?>



    <!-- рецепты -->
    <div class="row">
        <div class="col-md-7">
            <div class="recept_main_picture">
                <img src="<?= $model->image ?>" style="ma-height:500px;max-width:100%;">
            </div>
        </div>
        <div class="col-md-5 recept_desc">
            <h1 ><?= $model->name ?></h1>

            <p><?= $model->short_description ?></p>

            <div class="share_block">
                <div id="maxShare">
                    <div id="facebook" data-url="" data-text="Нежный кофейный пирог от Филиппа Контичини" data-title="Share"></div>

                    <div id="comment">
                        <div class="box"><a class="count" name="comment_count" href="#comments">
                                <?= $model->count_comment ?>
                            </a>
                            <a class="share" href="#comments">COMMENT</a>
                        </div>
                    </div>

                    <div id="views">
                        <div class="box">
                            <a class="count" href="#">
                                <?= $model->count_view ?>
                            </a>
                            <a class="share" href="#">views</a>
                        </div>
                    </div>

                    <div id="shares">
                        <div class="box"><a class="count" href="#">
                                <?= $model->count_like ?>
                            </a>
                            <a class="share" href="#">shares</a>
                        </div>
                    </div>

                </div>



            </div>
        </div>

    </div>
<!-- рецепты -->

</div>


<div class="slider visible-md visible-lg">
    <div class="container">
        <div class="row" style="margin-top:20px;">
            <div class="col-md-12">
                <div id="recept_slider" class="carousel slide"  data-interval="false" data-ride="carousel">
                    <!-- Содержимое слайдов -->
                    <div class="carousel-inner">
                        <?
                            $count = $dataProvider->getTotalCount();

                            for($i = 0; $i<=$count/4; $i++)
                            {
                                echo $this->render(
                                    'gallery_row',
                                    [
                                        'dataProvider' => $dataProvider,
                                        'page' => $i
                                    ]
                                );
                            }
                        ?>
                    </div>

                    <!-- Controls -->

                    <a class="left carousel-control" href="#recept_slider" data-slide="prev">
                        <span class="slide_left_ico"></span>
                    </a>
                    <a class="right carousel-control" href="#recept_slider" data-slide="next">
                        <span class="slide_right_ico"></span>
                    </a>

                    <div class="clearfix"></div>

                    <div class="slide_name_block">
                        <span class="photo_num"><?= $count ?> фото</span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="Instruction">
            <h2><?= $model->name ?></h2>
            <div class="recipes_text">
                <?= $model->description ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div>
            <?= $this->render('/course/subscribeForm') ?>
        </div>
        <div class="btnSocial">
            <?= $this->render('/site/socialButtons',
                ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
        </div>
    </div>
</div>
<div class="container">



    <div class="col-md-3 hide">
        &nbsp;1
    </div>

    <div class="col-md-9">
        <h3 id="comments">Комментарии</h3>

    </div>


    <div class="col-md-3 hide">
        &nbsp;2
    </div>
</div>