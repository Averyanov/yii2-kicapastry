<?
use yii\widgets\ListView;

$this->title = Yii::t('app', 'recipes');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-9">
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-md-10">

                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="search" class="form-control btn-md" id="searchRecipe" name="sn" placeholder="Поиск рецептов">
                </div>
                <small>Пример поиска:
                    <span class="link-dashed" onclick="$('#searchRecipe').val(this.innerHTML)">выпечка</span>,
                    <span class="link-dashed" onclick="$('#searchRecipe').val(this.innerHTML)">бисквиты</span>
                </small>
            </div>
            <div class="col-md-2"><button type="submit" class="btn btn-primary">Найти</button></div>

            <div class="pull-right">
                <small>Сортировка по: &nbsp;&nbsp;<span class="link-dashed">дате</span> <span class="caret"></span></small>
            </div>
        </div>
    </form>

    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'tag' => null,
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-sm-4'
        ],
        'itemView' => 'recipeItem'
    ])
    ?>
</div>
<div class="col-md-3">
    <h3>Рецепты по разделам</h3>
    <div style="line-height:30px;margin-bottom:20px;">
        <a href="/recipes/?sn=глазурь" class="btn btn-primary btn-sm">глазурь</a>
        <a href="/recipes/?sn=шоколад" class="btn btn-primary btn-sm">шоколад</a>
        <a href="/recipes/?sn=декор" class="btn btn-primary btn-sm">декор</a>
        <a href="/recipes/?sn=выпечка" class="btn btn-primary btn-sm">выпечка</a>
        <a href="/recipes/?sn=пирожное" class="btn btn-primary btn-sm">пирожное</a>
        <a href="/recipes/?sn=макаронс" class="btn btn-primary btn-sm">макаронс</a>
        <a href="/recipes/?sn=эклер" class="btn btn-primary btn-sm">эклер</a>
        <a href="/recipes/?sn=бисквит" class="btn btn-primary btn-sm">бисквит</a>
        <a href="/recipes/?sn=песочное тесто" class="btn btn-primary btn-sm">песочное тесто</a>
        <a href="/recipes/?sn=торт" class="btn btn-primary btn-sm">торт</a>
    </div>
    <div>
        <?= $this->render('/course/subscribeForm') ?>
    </div>
    <div class="btnSocial">
        <?= $this->render('/site/socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
    </div>
</div>
