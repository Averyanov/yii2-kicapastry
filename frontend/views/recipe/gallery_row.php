<?
use yii\widgets\ListView;
use yii\helpers\Html;

$dataProvider->pagination->page = $page;
$dataProvider->refresh();


?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => [
        'class' => ['item', 'row', !$page ? 'active' : '']
//        'class' => ['item', 'row', 'active']
    ],
    'itemOptions' => [
        'class' => [
            'slide_block',
            'col-md-3',
            'col-sm-4',
            'col-xs-12'
        ]
    ],
    'itemView' => function($model) use ($dataProvider){
        return Html::a(
            false,
            $model->image,
            [
                'style' => "background-image: url($model->image)",
                'rel' => 'galleryBox'
            ]);
    }

])?>