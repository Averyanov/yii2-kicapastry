<?
use common\components\CUrl;
?>
<a href='<?= CUrl::to(['recipe/view', 'id' => $model->id]) ?>'>
    <div class='recipeBlock' style='background-image:url(<?= $model->image ?>);background-size: cover; background-position: 50% 50%;'>
        <div class='recipeCover col-md-5'>
            <div class='recipeTitle'><?= $model->name ?></div>
            <div class="text-right recipe-cart-wrap">
<!--                <form action="">-->
<!--                    <button class>some</button>-->
<!--                </form>-->
                <span class="glyphicon glyphicon-shopping-cart"></span>
            </div>
            <div style='position:absolute; bottom:15px; left:0;font-size:10pt;width:90%;'>
                <div class='text-left col-xs-6'>
                    <?= Yii::$app->formatter->asDate($model->date) ?>
                </div>
                <div class='text-right col-xs-6'>
                    <span class='glyphicon glyphicon-comment'></span> <?= $model->count_comment ?>
                    <span class='glyphicon glyphicon-eye-open'></span> <?= $model->count_view ?>
                </div>
            </div>
        </div>
    </div>
</a>