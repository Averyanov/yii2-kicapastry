<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\components\CUrl;
?>

<? ActiveForm::begin(['action' => CUrl::to(['/cart/discount'])]) ?>

<?= Html::submitButton('Использовать скидку в другой раз', [
    'class' => 'btn btn-danger',
    'disabled' => !$wasDiscountUsed
]) ?>

<? ActiveForm::end() ?>
