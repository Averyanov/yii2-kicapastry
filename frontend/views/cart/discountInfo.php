<div class="row">
    <div class="col-md-12">
        <h3 class="h3 text-danger">
            <?=
            $disCount
                ? Yii::t(
                    'app',
                    'У Вас есть {n, plural, one{# скидка} other{# скидки}}!',
                    ['n' => $disCount])
                : ''
            ?>
        </h3>
    </div>
</div>
<div class="row" style="padding:20px 25px;">
    <div class="col-sm-5" style="padding:0">
        <?=
            !$discount
                ? $this->render('discountForm', ['wasDiscountUsed' => $discount > 0])
                : $this->render('disableDiscountForm', ['wasDiscountUsed' => $discount > 0])
        ?>
    </div>
    <div class="col-sm-7 total">
        <span class="total_lbl">К оплате <?= $itemCount ?> видео: </span>
        <span class="total_price"><?= Yii::$app->formatter->asDecimal($total) ?> €</span><br>
    </div>
</div>