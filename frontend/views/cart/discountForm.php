<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\components\CUrl;
?>

<? ActiveForm::begin(['action' => CUrl::to(['/cart/discount'])]) ?>

<?= Html::submitButton(Yii::t('app', 'use_discount'), [
    'class' => 'btn btn-primary',
    'disabled' => $wasDiscountUsed,
    'title' => $wasDiscountUsed ? 'К сожалению, использовать можно не более одной скидки за заказ' : false
]) ?>

<? ActiveForm::end() ?>
