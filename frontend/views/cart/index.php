<?
use yii\widgets\ListView;
use yii\widgets\Breadcrumbs;
use common\components\CUrl;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'cart');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => [
            'label' => 'Международная Кулинарная Академия',
            'url'   => CUrl::to(['site/index'])
        ]
    ]) ?>
    <div class="col-md-9">
        <div style="margin-bottom:30px; height:50px">
            <div class="steps active_step">
                <span><strong><?= Yii::t('app', 'step') ?> 1</strong><br>
                <?= Yii::t('app', 'ordering') ?>
                </span>
            </div>
            <div class="steps">
                <span><strong><?= Yii::t('app', 'step') ?> 2</strong><br>
                <?= Yii::t('app', 'payment') ?>
                </span>
            </div>
            <div class="steps">
                <span><strong><?= Yii::t('app', 'step') ?> 3</strong><br>
                <?= Yii::t('app', 'getting_access') ?>
                </span>
            </div>
            <br>
        </div>
        <div>
        <span>
            <h3 class="list_title">
                <?= $dataProvider->getCount()
                    ? Yii::t('app',
                        'you_added_n_mc',
                        ['n' => $dataProvider->getCount()])
                    : false
                ?>
            </h3>
        </span>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => ['tag' => null],
            'itemOptions' => ['tag' => null],
            'itemView' => function($model) use ($discount) {
                return $this->render('/course/cartItem', [
                    'model'     => $model,
                    'discount'  => $discount
                ]);
            },
            'emptyText' => 'В корзине пока нет мастер-классов',
            'emptyTextOptions' => ['tag' => 'h3']
        ])
        ?>

        <?=
        $this->render('discountInfo', [
            'itemCount' => $dataProvider->getCount(),
            'disCount'  => Yii::$app->user->identity->discount,
            'discount'  => $discount,
            'total'     => $total
        ])
        ?>
    </div>
    <div class="col-md-3">
        <div class="info_block">
            <span class="list_ico"></span>
            <p class="info_text">Вы получите моментальный доступ к оплаченным записям!</p>
        </div>
        <div class="info_block">
            <span class="list_ico"></span>
            <p class="info_text">Вы сможете наблюдать за мастер-классом и задавать вопросы в режиме реального времени!</p>
        </div>

        <div class="pay_start_block">
            <? $form = ActiveForm::begin(['action' => CUrl::to(['cart/payment'])]); ?>
                <span class="total_sum_lbl">Сумма к оплате:</span>
                <span class="total_sum_price"><?= Yii::$app->formatter->asDecimal($total) ?> €</span>
            <?= Html::submitButton('Перейти к оплате', [
                'class' => 'btn go_pay',
                'name'  => 'go_pay',
                'disabled' => !$dataProvider->count
            ]) ?>
            <span class="total_sum_info">или вернуться к </span>
            <a href='<?= CUrl::to(['course/online']) ?>' class="total_sum_back">каталогу видео</a>
            <? ActiveForm::end(); ?>
<!--            <form action="/cart-payment/" method="post">-->
<!--                <span class="total_sum_lbl">Сумма к оплате:</span>-->
<!--                <span class="total_sum_price">--><?//= Yii::$app->formatter->asDecimal($total) ?><!-- €</span>-->
<!--                <input class="btn go_pay" type="submit" name="go_pay" value="Перейти к оплате">-->
<!--                <span class="total_sum_info">или вернуться к </span>-->
<!--                <a href='--><?//= CUrl::to(['course/online']) ?><!--' class="total_sum_back">каталогу видео</a>-->
<!--            </form>-->

        </div>
    </div>
</div>
<div class="container payment_description">
    <div class="col-sm-1 col-sm-offset-2">
        <img src="/image/question_ico.png">
    </div>
    <div class="col-sm-9" style="padding-left:30px">
        <div class="phone_block">
            <h3 style="margin-top:0;">Необходима техническая помощь с оплатой?</h3>
            Напишите нам на почту <a href="mailto:webinar.kica@gmail.com">webinar.kica@gmail.com</a> или позвоните по телефону:<br>
            <table cellpadding="5" cellspacing="0">
                <tbody><tr>
                    <td>Россия:</td>
                    <td>+7 499 350 26 86</td>
                </tr>
                <tr>
                    <td>Украина:</td>
                    <td>+38 063 237 01 10, <br>
                        +38 044 237 01 10</td>
                </tr>
                </tbody></table>
        </div>
    </div>
</div>