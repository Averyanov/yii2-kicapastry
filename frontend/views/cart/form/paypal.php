<div class="text-center col-sm-6 col-xs-12 col-md-3">
    <form action="<?= $payment->url->action ?>" method="post">
        <input name="cmd" type="hidden" value="_xclick">
        <input name="business" type="hidden" value='<?= $payment->email ?>'>
        <input name="item_name" type="hidden" value="Order #<?= $order->id ?>">
        <input name="item_number" type="hidden" value="<?= $order->id ?>">
        <input name="amount" type="hidden" value="<?= $price ?>">
        <input name="no_shipping" type="hidden" value="1">
        <input name="rm" type="hidden" value="2">
        <input name="currency_code" type="hidden" value="<?= $payment->currency ?>">
        <input name="return" type="hidden" value="<?= $payment->url->success ?>">
        <input name="cancel_return" type="hidden" value="<?= $payment->url->return ?>">
        <input name="notify_url" type="hidden" value="<?= $payment->url->notify ?>">
        <input type="image" value="Платить через PayPal" src="http://elmoney.net/upload/iblock/43d/43d6bcf12adf26c25b30316eae660c35.png" width="110">
    </form>
</div>