<?
use common\components\CUrl;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

$this->title = 'Оплата заказа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container" style="min-height:600px;">

    <div class="col-md-9">
        <div style="margin-bottom:30px; height:50px">
            <div class="steps">
                    <span><strong>Шаг 1</strong><br>
                    Оформление заказа
                    </span>
            </div>
            <div class="steps active_step2">
                    <span><strong>Шаг 2</strong><br>
                    Оплата
                    </span>
            </div>
            <div class="steps">
                    <span><strong>Шаг 3</strong><br>
                    Получение доступа
                    </span>
            </div>
            <br>
        </div>

        <div>

            <span class="payment_list_title">Проверьте Ваш заказ:</span>
        </div>


        <div class="text-center lead row">
            <a href='<?= CUrl::to(['cart/payment', 'post_action' => 'cancel']) ?>' class="btn btn-danger pull-right">Изменить заказ</a>
            <div class="alert-info col-md-8 col-md-offset-2" style="padding:20px;">
                Заказ номер <?= $order->id ?>, к оплате  <span class="total_price"><?= $total ?> €</span>
            </div>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => null,
            'itemOptions' => null,
            'itemView' => '/course/payment_item'
        ])
        ?>
        <br>
        <h3>Выберите способ оплаты:</h3>


        <div class="row" style="min-height:160px;margin-top:5px;margin-bottom:20px;">
            <?= $this->render('form/apioplata',['disabled' => true]) ?>
            <?= $this->render('form/liqpay',['disabled' => true]) ?>
<!--            <div class="text-center col-sm-6 col-xs-12 col-md-3">-->
<!--                <a href="/payment/confirm/?order=234566"><input type="image" src="/img/payment4.png"></a>-->
<!--            </div>-->
            <?= $this->render('form/paypal',[
                'order' => $order,
                'payment' => $payment,
                'price' => $total
            ]) ?>
        </div>

    </div>


    <div class="col-md-3">
        <div class="payment_info_block">
            <center>
                <img src="/img/question_black.png">
            </center>
            <h3>Необходима техническая помощь с оплатой?</h3>
            Напишите нам на почту <a href="mailto:webinar.kica@gmail.com">webinar.kica@gmail.com</a><br>или позвоните по телефону:<br>
            <table>
                <tbody><tr>
                    <td>Россия:</td>
                    <td>+7 499 350 26 86</td>
                </tr>
                <tr>
                    <td>Украина:</td>
                    <td>+38 063 237 01 10<br>
                        +38 044 237 01 10</td>
                </tr>


                </tbody></table>
        </div>
    </div>

</div>