<?
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Вопросы и ответы';
$this->params['breadcrumbs'][] = 'FAQ';
?>
<div class="row">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'options' => [
            'tag' => 'div',
            'class' => 'col-lg-9 col-md-8 col-sm-7'
        ],
        'itemOptions' => ['tag' => null],
        'itemView' => function($model)
        {
            return
                Html::tag(
                    'h3',
                    Html::tag('i',null,['class' => 'glyphicon glyphicon-question-sign']).
                    ' '.$model->name
                ).Html::tag(
                    'div',
                    Html::tag('i', null, ['class' => 'icon-comment']).
                    Html::tag('p', $model->content)
                );

        }
    ])
    ?>
    <div class="col-lg-3 col-md-4 col-sm-5">
        <div>
            <?= $this->render('/course/subscribeForm') ?>
        </div>
        <div class="btnSocial">
            <?= $this->render('/site/socialButtons', ['class' => 'col-sm-6 text-center social-follow-ico']) ?>
        </div>
    </div>
</div>