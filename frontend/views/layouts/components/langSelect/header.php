<?
use yii\bootstrap\Dropdown;
use common\models\Lang;
?>
<div class="dropdown">
    <button class="langSelect btn dropdown-toggle"
            type="button"
            id="langSelect"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="true">

        <img src='<?= Yii::$app->session['_lang._model']->image ?>' width="24" height="16" hspace="5"/>
        <?= Yii::$app->session['_lang._model']->name ?>
        <span class="caret"></span>

    </button>
    <?= Dropdown::widget([
        'items' => Lang::getSelectItems(),
        'encodeLabels' => false
    ]) ?>
</div>
