<?
use common\models\Lang;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?
$language = $model->local == Lang::DEFAULT_LOCAL
    ? null
    : $model->Clocal;



$is_current = $model->local == Lang::getCurrentLocal(true);
$url = ($is_current)
    ? Url::to('#')
    : Url::current(['language' => $language]);
?>

<?
$content = Html::img($model->image, ['style' => 'width:24px; height:16px; margin-right:5px;']);
$content .= $model->name;
?>

<?=
Html::a($content, $url, ['class' => $is_current ? 'hidden' : 'btn-link']);
?>
