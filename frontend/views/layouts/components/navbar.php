<?
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\Course;
use common\components\CUrl;
?>
<?// TODO  get main nav ?>
<nav class="navbar navbar-kica">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">
                    <?= Yii::t('app', 'toggle_navigation') ?>
                </span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">

                <?/*<li>
                    <a href='<?= CUrl::to('course/broadcast') ?>'>
                        <img src="/image/icon-broadcast.png" class="icon-kica">
                        <?= Yii::t('app', 'broadcasting_of_courses') ?>
                    </a>
                </li>*/?>
                <li class="dropdown">
                    <a class="dropdown-toggle disabled" data-toggle="dropdown" href='<?= CUrl::to(['course/online']) ?>' role="button" aria-haspopup="true" aria-expanded="true">
                        <?= Yii::t('app', 'all_online_courses') ?>
                         <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?=
                        ListView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Course::find()->where(['<>', 'price', 0]),
                                'pagination' => [
                                    'pageSize' => 6
                                ]
                            ]),
                            'layout' => '{items}',
                            'options' => [
                                'tag' => null
                            ],
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => 'clearfix'
                            ],
                            'itemView' => function($model)
                            {
                                return Html::a(
                                    Html::img($model->image, ['width' => 50, 'style' => 'margin: 5px 10px;', 'class' => 'pull-left']).
                                    Html::tag('div', $model->name, ['style' => 'padding: 5px 0']),
                                    CUrl::to(['course/view', 'url' => $model->url])
                                );
                            }
                        ])
                        ?>
                        <li style="border-top: 1px solid #d7d7d7;">
                            <a href='<?= CUrl::to(['course/online']) ?>' class="dropdown-showall">
                                <?= Yii::t('app', 'show_all_online') ?>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle disabled" data-toggle="dropdown" href='<?= CUrl::to(['course/online', 'attr' => 'price', 'value' => 0]) ?>' role="button" aria-haspopup="true" aria-expanded="true">
                        <?= Yii::t('app', 'free_online_courses') ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?=
                        ListView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Course::find()->where(['price' => 0]),
                                'pagination' => [
                                    'pageSize' => 6
                                ]
                            ]),
                            'layout' => '{items}',
                            'options' => [
                                'tag' => null
                            ],
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => 'clearfix'
                            ],
                            'itemView' => function($model){
                                return Html::a(
                                    Html::img($model->image, ['class' => 'pull-left', 'style' => 'width:50px;margin:5px 10px;'])
                                    .Html::tag('div', $model->name, ['style' => 'padding:5px 0px;']),
                                    CUrl::to(['course/view', 'url' => $model->url])
                                );
                            }
                        ])
                        ?>
                        <li style="border-top: 1px solid #d7d7d7;">
                            <a href='<?= CUrl::to(['course/online', 'attr' => 'price', 'value' => 0]) ?>' class="dropdown-showall">
                                <?= Yii::t('app', 'all_free_videos') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href='<?= CUrl::to('recipe/index') ?>'>
                        <?= Yii::t('app', 'recipes') ?>
                    </a>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right navbar-login">
                <?= $this->render(Yii::$app->user->isGuest ? 'guest' : 'user') ?>
            </ul>

        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
<?//    NavBar::begin([
//        'brandLabel' => 'My Company',
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);
//    $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
//    ];
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//    } else {
//        $menuItems[] = '<li>'
    //            . Html::beginForm(['/site/logout'], 'post')
    //            . Html::submitButton(
    //                'Logout (' . Yii::$app->user->identity->username . ')',
    //                ['class' => 'btn btn-link logout']
    //            )
    //            . Html::endForm()
    //            . '</li>';
//    }
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => $menuItems,
//    ]);
//    NavBar::end();