<?
use common\components\CUrl;
?>
<li class="dropdown">
    <a style="line-height: 15px;" class="dropdown-toggle" data-toggle="dropdown" href='#' role="button" aria-haspopup="true" aria-expanded="true">
        <div class="topmenu-topline">
            <?= Yii::$app->getUser()->identity->username ?>,
        </div>
        <div class="topmenu-bottomline">
            <?= Yii::t('app', 'your_account') ?>
            <span class="caret"></span>
        </div>
    </a>
    <ul class="dropdown-menu dropdown-menu-right" style="display: none;">
        <li class="clearfix">
            <a href='<?= CUrl::to(['user/index']) ?>'>
                <img src="/image/my_records.png" class="pull-left">
                <div>
                    <?= Yii::t('app', 'personal_account') ?>
                </div>
            </a>
        </li>
        <li>
            <a href='<?= CUrl::to(['user/settings']) ?>'>
                <img src="/image/preferences.png" class="pull-left">
                <div>
                    <?= Yii::t('app', 'settings') ?>
                </div>
            </a>
        </li>
        <li>
            <a href='<?= CUrl::to(['user/logout']) ?>'>
                <img src="/image/logout.png" class="pull-left">
                <div>
                    <?= Yii::t('app', 'logout') ?>
                </div>
            </a>
        </li>
    </ul>
</li>
<li>
    <a style="line-height: 15px;" href='<?= CUrl::to('favorite/index') ?>'>
        <?= Yii::t('app', 'wish_list') ?>
    </a>
</li>
<li>
    <a href='<?= CUrl::to('cart/index') ?>' style="padding-left: 10px; padding-right: 15px;">
        <div style="width:35px; height:30px; background-image: url(/image/icon-cart.png); background-repeat: no-repeat; background-position: bottom;">
            <span style="position: relative; left: 17px; top: -10px; font-size: 16px; color:#e5e0a9; font-weight: 500;">
                <?= $cart_count = Yii::$app->user->identity->getCartItems()->where(['status' => 0])->count() ?: '' ?>
            </span>
            <span style="position: relative; left: 17px; top: -18px; font-size: 15px; color:#480000; font-weight: bold;">
                <?= $cart_count = Yii::$app->user->identity->getCartItems()->where(['status' => 1])->count() ?: '' ?>
            </span>
        </div>
    </a>
</li>