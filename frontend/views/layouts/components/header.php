<?
use common\components\CUrl;
?>
<div class="header">
    <?/*
    if(Yii::$app->language = 'en')
        echo $this->render('/layouts/components/jumpPopOut/form')
    */?>
    <div class="container">
        <div class="col-sm-3 col-md-3 text-right header-phone visible-xs">
            <? //TODO  phone number ?>
            <div>+380 (94) 853 30 54</div>
            <a href='<?= CUrl::to('/contact') ?>'>
                <p>
                    Обратная связь
                    <?
                    // TODO feedback
                    // {$t.topmenu_feedback}
                    ?>
                </p>
            </a>
        </div>
        <?= $this->render('langSelect/header') ?>
        <div class="logo col-sm-3 col-md-3">
            <a href='<?= CUrl::to('site/index') ?>'>
                <img src="/image/kica-logo.png"/>
            </a>
        </div>
        <div class="col-sm-6 col-md-7 text-center">
            <?// TODO   get nav?>
            <ul class="nav navbar-nav navbar-left navlogo">


                <li><a href='<?= CUrl::to('chef/index') ?>'><?= Yii::t('app', 'chefs') ?></a></li>
                <li><a href='<?= CUrl::to('faq/index') ?>'><?= Yii::t('app', 'faq') ?></a></li>
                <li><a href='<?= CUrl::to('gallery/index') ?>'><?= Yii::t('app', 'gallery') ?></a></li>

            </ul>
        </div>
        <div class="col-sm-3 col-md-2 text-right header-phone hidden-xs">
            <div class="text-nowrap">
                <? //TODO  phone number ?>
                <div>+380 (94) 853 30 54</div>
            </div>
            <a href='<?= CUrl::to('site/contact') ?>'>
                <p>
                    <?= Yii::t('app', 'feedback') ?>
                </p>
            </a>
        </div>
        <div class="tl-call-catcher"> <!--BANNER ON SITE--> </div>
    </div>
</div>