<?
use yii\helpers\Html;
?>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="/favicon.ico?v=1" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= Html::encode($this->title)?></title>
    <?// TODO Meta Tags?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
    <? $this->head() ?>
</head>