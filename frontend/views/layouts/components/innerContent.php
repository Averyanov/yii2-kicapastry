<?
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\components\CUrl;
?>
<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => [
            'label' => Yii::t('app', 'ica'),
            'url'   => CUrl::to(['site/index'])
        ]
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>