<?
use common\components\CUrl;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\Lang;
use yii\helpers\Html;
?>
<footer class="footer">
    <div class="container">
        <div class="col-md-5">
            © <?= date('Y') ?>.
            <?= Yii::t('app', 'copyright') ?>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Lang::find()
            ]),
            'layout' => '{items}',
            'options' => [
                'class' => 'col-md-2'
            ],
            'itemOptions' => [
                'style' => 'display:block;clear:both;'
            ],
            'itemView' => 'langSelect/footer'
        ])
        ?>
        <div class="col-md-5 text-right">
            <ul class="list-inline">
                <li>
                    <a href='<?= CUrl::to(['site/contact']) ?>'>
                        <?= Yii::t('app', 'contacts') ?>
                    </a>
                </li>
                <li>
                    <a href='<?= CUrl::to(['site/about']) ?>'>
                        <?= Yii::t('app', 'about') ?>
                    </a>
                </li>
                <li>
                    <a href='<?= CUrl::to(['site/policy']) ?>'>
                        <?= Yii::t('app', 'privacy_policy') ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>