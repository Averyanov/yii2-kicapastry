<?
use frontend\assets\AppAsset;
?>

<?php
/* @var $this \yii\web\View */
/* @var $content string */
//
?>
<? $this->beginPage() ?>
<?
AppAsset::register($this);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<?= $this->render('components/head')?>
<body>
<? $this->beginBody() ?>
<?//= $this->render('components/onlineLine') ?>
<?= $this->render('components/header')?>
<?= $this->render('components/navbar') ?>
<?= $this->render('components/mainContent', ['content' => $content]) ?>
<?= $this->render('components/footer') ?>
<? $this->endBody() ?>
</body>
</html>
<? $this->endPage(); ?>
