<?

namespace common\components;

use Yii;
use common\models\Lang;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/* @var $app \yii\web\Application */

class LangSelector implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if($app->session['_lang'])
            $app->language = $app->session['_lang'];

        $request = $app->urlManager->parseRequest($app->getRequest());
        $language = ArrayHelper::getValue($request, '1.language');

        $app->session['_lang'] = empty($language)
            ? Lang::DEFAULT_LOCAL
            : ['en' => 'en-US', 'es' => 'es-ES'][$language];
        $app->language = $app->session['_lang'];
        $app->session['_lang._model'] = Lang::getCurrent();
    }
}