<?

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class GalleryModelBehavior extends Behavior
{
//    public
//        $imageInstance,
//        $oldFile = null;

    public $itemClassName;

    private $instances = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'linkItems',
            ActiveRecord::EVENT_AFTER_UPDATE => 'linkItems',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
        ];
    }

    public function beforeValidate()
    {
        return $this->uploadImages();
    }

    public function uploadImages()
    {
        $images = UploadedFile::getInstances($this->owner, 'images');

        $hasMain = $this->owner->getMain();

        if($images)
        {
            foreach ($images as $key => $image)
            {
                $itemModel = Yii::createObject($this->itemClassName);
                $itemModel->image = $this->getPath($image);
                $itemModel->status = 1;
                $itemModel->is_main = ($key == 0 && !$hasMain);
                $this->instances[$key] = [
                    'model' => $itemModel,
                    'image' => $image
                ];

            }
        }
//        VarDumper::dump($this->instances,10,1);die;

//        if($image)
//        {
//            $this->oldFile = $this->owner->isNewRecord
//                ? nulluse yii\helpers\VarDumper;
//                : $this->owner->getOldAttribute('image');
//
//            $this->owner->_image = $image->name;
//            $this->imageInstance = $image;
//        }
//        else
//            $this->owner->image = $this->owner->getOldAttribute('image') . "";

    }

    public function preparePath($extension)
    {
        return Yii::$app->params['uploadPath' . StringHelper::basename($this->owner->className())] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
    }

    public function getPath($image)
    {
        return $this->preparePath(explode('/', $image->type)[1]);
    }

    public function linkItems()
    {
//        VarDumper::dump($this->instances,10,1);die;
        foreach ($this->instances as $itemInstances)
        {
            $itemInstances['model']->link('owner', $this->owner);
            $itemInstances['image']->saveAs(ltrim($itemInstances['model']->image, '/'));
        }
//        Image::thumbnail(
//            $this->imageInstance->tempName,
//            $this->owner->width,
//            $this->owner->height
//        )
//            ->save(ltrim($this->owner->image, '/'), ['quality' => 80]);
        return ['some' => 'value'];
    }

    public function getInitialPreview($params)
    {
        return ArrayHelper::getColumn($this->owner->galleryItems, $params);
//        return isset($attr['column'])
//            ? ArrayHelper::getColumn($this->owner->galleryItems, $attr['column'])
//            : (isset($attr['config']))
//                ? (ArrayHelper::getColumn($this->owner->galleryItems, function($model) use ($attr) {
////                    return [
////                        'key' => $model->id,
////                        'url' => 'gallery/delete-image',
////                        'extra' => [
////                            'id' => $model->owner_id
////                        ]
////                    ];
//                    return $attr['config'];
//                }))
//                : null;
    }

//    public function getInitialPreviewThumbTags()
//    {
//        return ArrayHelper::getColumn(
//            $this->owner->galleryItems,
//
//        );
//    }

    public function beforeDelete()
    {
        return $this->deleteImages();
    }

    public function deleteImages()
    {
        foreach($this->owner->galleryItems as $image)
            $image->delete();
    }

    public function getOptions()
    {
        return [
            'options' => [
                'accept'=>'image/*',
                'multiple' => true
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['pre-upload']),
                'allowedFileExtensions'=>['jpg','gif','png'],
                'initialPreview' => $this->getInitialPreview('image'),
                'initialPreviewAsData' => true,
                'initialPreviewConfig' => $this->getInitialPreview($this->getConfig()),
                'initialPreviewThumbTags' => $this->getInitialPreview($this->getThumbTags()),
                'overwriteInitial' => false,
                'layoutTemplates' => [
                    'footer' => Yii::$app->controller->renderPartial(
                        $this->owner->isNewRecord
                            ? '/FInput/footer/create'
                            : '/FInput/footer/update'
                    )
                ],
                'deleteUrl' => Url::to(['delete-image']),
                'uploadAsync' => false,
//                'uploadExtraData' => new \yii\web\JsExpression('function(){return {gallery_id: id};}'),
                'uploadExtraData' => $this->owner->isNewRecord
                    ? new \yii\web\JsExpression('function(){return {gallery_id: id};}')
                    : ['gallery_id' => $this->owner->id],
                'browseOnZoneClick' => true,
                'showBrowse' => false,
                'showRemove' => false,
                'showUpload' => false
            ]
        ];
    }

    public function getConfig()
    {
        return function ($model)
        {
            return [
                'key' => $model->id,
                'url' => Url::to(['delete-image']),
            ];
        };
    }

    public function getThumbTags()
    {
        return function($element)
        {
            return [
                '{MAIN_BTN_BG}'         => $element->is_main    ? 'info' : 'default',
                '{MAIN_ICON_TEXT}'      => $element->is_main    ? 'danger' : 'muted',
                '{MAIN_TITLE}'          => $element->is_main    ? 'Главное изображение' : 'Сделать главным',
                '{MAIN_BTN_CLASS}'      => $element->is_main    ? 'disabled' : null,
                '{MAIN_BTN_LISTENER}'   => $element->is_main    ? 'return false;' : 'setProp(this)',
                '{STATUS_ICON}'         => $element->status     ? 'open' : 'close',
                '{STATUS_ICON_TEXT}'    => $element->status     ? 'primary' : 'muted',
                '{STATUS_TITLE}'        => $element->status     ? 'видимое' : 'невидимое',
                '{ID}'                  => $element->id,
            ];
        };
    }
}