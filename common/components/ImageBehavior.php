<?

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\helpers\StringHelper;

class ImageBehavior extends Behavior
{
    public
        $imageInstance,
        $oldFile = null,
        $img_name_lvl = 4,
        $img_pre_path = '/backend/web';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
        ];
    }

    public function uploadImage()
    {
        $image = UploadedFile::getInstance($this->owner, 'image');

        if($image)
        {
            $this->oldFile = $this->owner->isNewRecord
                ? null
                : $this->owner->getOldAttribute('image');

            $this->owner->_image = $image->name;
            $this->imageInstance = $image;
        }
        else
            $this->owner->image = $this->owner->getOldAttribute('image') . "";

    }

    public function deleteImage()
    {
        foreach($this->owner->img_size as $size_name => $params)
            unlink(ltrim($this->owner->getImage($size_name), '/'));

        return true;
    }

    public function preparePath($extension)
    {
        return Yii::$app->params['uploadPath' . StringHelper::basename($this->owner->className())] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
    }

    public function getOptions()
    {
        return [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>[
                'allowedFileExtensions'=>['jpg','gif','png', 'jpeg'],
                'previewFileType' => 'any',
                'initialPreview' => ($this->owner->isNewRecord || empty($this->owner->getImage('full'))) ? false : Url::to($this->owner->getImage('full'), true),
                'initialPreviewAsData'=>true,
                'showRemove' => false,
                'showUpload' => false
            ]
        ];
    }

    // events

    public function beforeValidate()
    {
        return $this->uploadImage();
    }

    public function afterInsert()
    {
        foreach($this->owner->img_size as $size_name => $params)
        {
            Image::thumbnail(
                $this->imageInstance->tempName,
                $this->owner->img_size[$size_name]['width'],
                $this->owner->img_size[$size_name]['height']
            )
                ->save(ltrim($this->owner->getImage($size_name), '/'), ['quality' => 80]);
        }
    }

    public function afterUpdate()
    {
        if($this->imageInstance)
        {
            $this->afterInsert();

            if($this->oldFile)
                foreach($this->owner->img_size as $size_name => $params)
                {
                    $image = ltrim($this->getImage($size_name, true), '/');
                    if(file_exists($image))
                        unlink($image);
                }
        }
    }

    public function beforeDelete()
    {
        return $this->deleteImage();
    }

    // properties

    public function set_Image($imageName)
    {
        $file_name = explode(".", $imageName);
        $ext = end($file_name);

        $path = $this->preparePath($ext);

        $this->owner->image = $path;

        return $path;
    }

    public function getImage($size_name = 'min', $is_old = false)
    {
        $image = $is_old ? $this->oldFile : $this->owner->image;

        $img_path_arr = explode('/', $image);

        if(count($img_path_arr) < $this->img_name_lvl)
            return null;

        $img_path_arr[$this->img_name_lvl] = $size_name . '_' . $img_path_arr[$this->img_name_lvl];


        $debug_name = implode(
            $this->img_pre_path,
            [
                dirname(__DIR__,2),
                implode('/', $img_path_arr)
            ]);

        return
            file_exists($debug_name)
                ? implode('/', $img_path_arr)
                : $this->owner->image;
    }

}