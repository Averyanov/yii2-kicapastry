<?

namespace common\components;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use common\models\Lang;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CMultilingualBehavior extends MultilingualBehavior
{
    public function getPrefix($local)
    {
        return $local !== Yii::$app->language ? '_'.$local : '';
    }

    public function getTabs($form)
    {
        $lang_model = Lang::find()->asArray()->all();
        $model = $this->owner;

        return  ArrayHelper::getColumn($lang_model, function($element) use ($model, $form){
            return [
                'label' => Html::img($element['image']),
                'encode' =>false,
                'content' => Yii::$app->controller->renderPartial(
                    'lang/_form',
                    [
                        'form' => $form,
                        'model' => $model,
                        'lang_prefix' => $model->getPrefix($element['Clocal'])
                    ])
            ];
        });
    }
}