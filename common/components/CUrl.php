<?
namespace common\components;

use common\models\Lang;
use Yii;
use yii\helpers\BaseUrl;
use yii\helpers\VarDumper;

class CUrl
{
    public static function to($url = '', $scheme = false)
    {
        $url = array_merge(
            array_filter(['language' => Lang::getCurrentCustomizedLocal()], function($value){return $value;}),
            (is_array($url) ? $url : [$url])
        );
//        if($url[0] == 'course/online' && !empty($url['attr']))
//        {var_dump($url);die;}

        return BaseUrl::to($url, $scheme);
    }
}