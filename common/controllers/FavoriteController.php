<?php

namespace common\controllers;

use common\models\Cart;
use common\models\Course;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\components\CUrl;

class FavoriteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all liked models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
            return $this->goHome();

        $dataProvider = new ActiveDataProvider([
            'query' => Yii::$app->user->identity->getFavoriteCourses()
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDelete($id)
    {
        Course::findOne($id)->getFavoriteItem()->delete();

        return $this->redirect(CUrl::to(['favorite/index']));
    }
}
