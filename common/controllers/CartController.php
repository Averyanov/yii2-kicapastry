<?php

namespace common\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Cart;
use common\models\User;
use common\models\Order;
use yii\web\NotFoundHttpException;
use common\components\CUrl;
use common\models\Payment;
use common\models\Lang;

class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                    'discount' => ['POST'],
//                    'payment' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Chef models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
            return $this->redirect('/user/login');

        $user = Yii::$app->user->identity;
        $order = $user->currentOrder ?: (new Order())->crelink();

        if($user->currentOrder && $user->currentOrder->status == Order::STATUS_SENT)
            return $this->redirect('cart/payment');

        $this->layout = 'main';

        Url::remember();



        $discount_persent = $order ? $order->discount_persent : 0;

//        $total -= $total * $discount_persent / 100;
        $total = $user->getTotalCartSum(true);

        $dataProvider = new ActiveDataProvider([
            'query' => $user->getCourses()
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'total' => $total,
            'discount' => $discount_persent
        ]);
    }

    /**
     * Displays a single Chef model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        if(Yii::$app->user->isGuest)
            return $this->goHome();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDiscount()
    {
        $user = Yii::$app->user->identity;

        $order = $user->currentOrder ?: (new Order())->crelink();

        if($order->discount_persent)
        {
            $order->discount_persent = 0;
            $user->discount = 1;
            $order->save();

        }
        else if($user->discount && !$order->discount_persent)
        {
            $order->discount_persent = Order::DISCOUNT_PERSENT;
            $user->discount = -1;
            $order->save();
        }

        return $this->redirect(CUrl::to(['cart/index']));
    }

    /**
     * Deletes an existing Chef model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id of course
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest)
            return $this->redirect(CUrl::to(['site/index']));

        $admin_id = User::ADMIN_ID;
        $user_id = Yii::$app->user->identity->id;

        $params = ($admin_id == $user_id)
            ? ['id' => $id]
            : ['course_id' => $id, 'owner_id' => $user_id];

        Cart::findOne($params)->delete();
        // TODO set flash with url to deleted course
        return $this->redirect(CUrl::to(['cart/index']));
    }

    public function actionPayment($post_action = false)
    {
        if(Yii::$app->user->isGuest)
            return $this->redirect(CUrl::to(['site/index']));

        $user = Yii::$app->user->identity;

        if(empty($user->cartItems))
            return $this->redirect(CUrl::to(['cart/index']));

        $order = $user->currentOrder;

        if($post_action == 'cancel')
        {
            $order->prepareToPay(Order::STATUS_CREATED);
            return $this->redirect(CUrl::to(['cart/index']));
        }

        $order->prepareToPay(Order::STATUS_SENT);

        $total = $order->getTotalCartSum(true);

        $dataProvider = new ActiveDataProvider(['query' => $order->getCourses()]);

        $payment = new Payment();

        return $this->render('/cart/payment',[
            'order' => $order,
            'dataProvider' => $dataProvider,
            'total' => $total,
            'payment' => $payment
        ]);
    }

    /**
     * Finds the Cart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
