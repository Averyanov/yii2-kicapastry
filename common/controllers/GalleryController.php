<?php

namespace common\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use common\models\Gallery;
use common\models\GallerySearch;
use common\models\GalleryItem;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Gallery::findOne($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getGalleryItems()->where(['status' => true])
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Gallery::find()->where(['id' => $id])->multilingual()->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $isOK = $this->findModel($id)->delete();
        Yii::$app
            ->getSession()
            ->setFlash(
                $isOK ? 'success'   : 'error',
                $isOK ? 'Удалено!'  : 'Запись не удалось удалить'
            );

        return $this->redirect(['index']);
    }

    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['success' =>GalleryItem::findOne(Yii::$app->request->post('key'))->delete()];

//        $trash = (isset($request['num']) && isset($request['id']))
//            ? (GalleryItem::find()
//                ->where(['owner_id' => $request['id']])
//                ->limit(1)
//                ->offset($request['num'])
//                ->one())
//            : (isset($request['key'])
//                ? (GalleryItem::findOne($request['key']))
//                : null);
//
//        return [
//            'id' => $trash->id,
//            'img' => $trash->image,
//            'success' => $trash->delete()
//        ];
    }

    public function actionSetMain()
    {
        $model = new GalleryItem();

        return $model->setAsMain(Yii::$app->request->post('id'));
    }

    public function actionSetStatus()
    {
        return GalleryItem::setStatus(Yii::$app->request->post('id'));
    }

    public function actionPreUpload()
    {
        $request = Yii::$app->request->post();
        $id = (int) $request['gallery_id'];

        $model = new Gallery();

        $gallery = $model->initForPreUpload($id);

        $gallery->load(Yii::$app->request->post());
        $gallery->save();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'gallery_id' => $gallery->id,
            'initialPreview' => $gallery->getInitialPreview('image'),
            'initialPreviewConfig' => $gallery->getInitialPreview($gallery->getConfig()),
            'initialPreviewThumbTags' => $gallery->getInitialPreview($gallery->getThumbTags()),
            'append' => false
        ];
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
