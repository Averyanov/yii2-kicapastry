<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualQuery;
use common\components\ImageBehavior;
use common\components\CMultilingualBehavior;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "course".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $price
 * @property string $image
 * @property string $start_date
 * @property string $end_date
 * @property integer $status
 *
 * @property Chef $owner
 * @property CourseLang[] $courseLangs
 */
class Course extends \yii\db\ActiveRecord
{
//    public
//        $width = 115,
//        $height = 115;
    public
        $img_size = [
            'min' => [
                'width' => 200,
                'height' => 150
            ],
            'full' => [
                'width' => 800,
                'height' => 600
            ]
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course';
    }

    public function behaviors()
    {
        return [
            ImageBehavior::className(),
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => CourseLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'chef_lang',
                'attributes' => [
                    'local_status',
                    'name',
                    'content',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['owner_id', 'status'], 'required'],
            [['owner_id', 'price', 'status', 'local_status'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png, jpeg', 'maxSize' => 1024 * 1024],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chef::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['name', 'url'], 'string', 'max' => 64],
            [['content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'price' => Yii::t('app', 'Price'),
            'image' => Yii::t('app', 'Image'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Chef::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseLangs()
    {
        return $this->hasMany(CourseLang::className(), ['owner_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CourseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        $this->price = (int) $this->price;
        return true;
    }

    public function getOnlineByCondition($attr, $value)
    {
        $custom_params[$attr] = $value;
        $default_params = ['status' => true];

        $params = $this->hasAttribute($attr)
            ? array_merge($default_params, $custom_params)
            : $default_params;

//        VarDumper::dump($this
//            ->find()
//            ->joinWith('translation')
//            ->where([
//                '<',
//                'start_date',
//                new Expression('NOW()')
//            ])
//            ->andWhere(['local_status' => true])
//            ->andFilterWhere($params)
//            ->orderBy(['start_date' => SORT_DESC]),10,1);die;

        return new ActiveDataProvider([
            'query' => $this
                ->find()
                ->joinWith('translation')
                ->where([
                    '<',
                    'start_date',
                    new Expression('NOW()')
                ])
                ->andWhere(['local_status' => true])
                ->andFilterWhere($params)
                ->orderBy(['start_date' => SORT_DESC]),
//              ->andWhere(['hasVideo'])  TODO append to course model some video field
            'pagination' => [
                'pageSize' => false
            ]
        ]);
    }

    public function get_IsLiked()
    {
        return Favorite::findOne([
            'owner_id'  => Yii::$app->user->identity,
            'course_id' => $this->id
        ]);
    }

    public function getFavoriteItem()
    {
        return $this->_isLiked;
    }

//    public function preparePath($extension)
//    {
//        return Yii::$app->params['uploadPathCourse'] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
//    }
}
