<?php

namespace common\models;

use Yii;
use common\components\CMultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use common\components\GalleryModelBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $status
 *
 * @property GalleryItem[] $galleryItems
 * @property GalleryLang[] $galleryLangs
 */
class Gallery extends \yii\db\ActiveRecord
{
    public $images = [];
        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    public function behaviors()
    {
        return [
            [
                'class' => GalleryModelBehavior::className(),
                'itemClassName' => GalleryItem::className()
            ],
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => GalleryLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'gallery_lang',
                'attributes' => [
                    'name',
                    'description',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['description', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 128],
            [['images'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024, 'maxFiles' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryItems()
    {
        return $this->hasMany(GalleryItem::className(), ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryLangs()
    {
        return $this->hasMany(GalleryLang::className(), ['owner_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public function initForPreUpload($id)
    {
        return $id
            ? $this->find()->where(['id' => $id])->one()
            : $this;
    }

    public function getMain()
    {
        return $this->getGalleryItems()->where(['is_main' => true])->one();
    }
}
