<?php

namespace common\models;

use Yii;
use common\components\CMultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "translation".
 *
 * @property integer $id
 * @property string $name
 *
 * @property TranslationLang[] $translationLangs
 */
class Translation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translation';
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => TranslationLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'chef_lang',
                'attributes' => [
                    'content'
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'content' => 'Значение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslationLangs()
    {
        return $this->hasMany(TranslationLang::className(), ['owner_id' => 'id']);
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public function getList($language)
    {
        $result = static::find()->multilingual()->asArray()->all();
        return ArrayHelper::map(
            $result,
            'name',
            function($item) use ($language){
                return ArrayHelper::index(
                    array_filter(
                        $item['translations'],
                        function($lang_item) use ($language){
                            return $lang_item['language'] == $language;
                        }),
                    'language'
                )[$language]['content'];
            }
        );
    }
}
