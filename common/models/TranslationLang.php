<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translation_lang".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $language
 * @property string $content
 *
 * @property Translation $owner
 */
class TranslationLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translation_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'language', 'content'], 'required'],
            [['owner_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 6],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Translation::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'language' => 'Language',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Translation::className(), ['id' => 'owner_id']);
    }
}
