<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $course_id
 * @property integer $status
 *
 * @property User $owner
 * @property Course $course
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
//        return [
//            [['owner_id', 'course_id', 'status'], 'required'],
//            [['owner_id', 'course_id', 'status'], 'integer'],
//            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
//            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
//        ];
        return [
            [['owner_id', 'status'], 'required'],
            [['owner_id', 'status'], 'integer'],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
//            'course_id' => 'Course ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getCourse()
//    {
//        return $this->hasOne(Course::className(), ['id' => 'course_id']);
//    }

    public function beforeSave($insert)
    {
        $favorite_model = Favorite::findOne([
            'owner_id' => $this->owner_id,
            'course_id' => $this->course_id
        ]);

        return $favorite_model
            ? $favorite_model->delete()
            : true;
    }
}
