<?php

namespace common\models;

use Yii;
use common\components\ImageBehavior;
use common\components\CMultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "chef".
 *
 * @property integer $id
 * @property integer $status
 * @property string $count_views
 * @property string $image
 *
 * @property ChefLang[] $chefLangs
 * @property Course[] $courses
 */
class Chef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public
        $img_size = [
            'min' => [
                'width' => 115,
                'height' => 115
            ],
            'full' => [
                'width' => 205,
                'height' => 205
            ]
        ];

    public function behaviors()
    {
        return [
            ImageBehavior::className(),
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => ChefLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'chef_lang',
                'attributes' => [
                    'name',
                    'rank',
                    'short_description',
                    'full_description',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ],
        ];
    }

    public static function tableName()
    {
        return 'chef';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['status'], 'integer'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
            [['count_views'], 'safe'],
            [['rank', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['short_description', 'full_description'], 'string'],
            [['name'], 'string', 'max' => 64]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'count_views' => Yii::t('app', 'Count Views'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChefLangs()
    {
        return $this->hasMany(ChefLang::className(), ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['owner_id' => 'id']);
    }

    public function getCourseDataProvider($params)
    {
        return new ActiveDataProvider([
            'query' => $this->getCourses()->where($params)
        ]);
    }

    /**
     * @inheritdoc
     * @return ChefQuery the active query used by this AR class.
     */
//    public static function find()
//    {
//        return new ChefQuery(get_called_class());
//    }
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

//    public function preparePath($extension)
//    {
//        return Yii::$app->params['uploadPathChef'] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
//    }

//    public function getPrefix($local)
//    {
//        return $local !== Yii::$app->language ? '_'.$local : '';
//    }

}
