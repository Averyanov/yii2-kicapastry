<?php

namespace common\models;

use Yii;
use common\components\CMultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property integer $status
 *
 * @property FaqLang[] $faqLangs
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => FaqLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'faq_lang',
                'attributes' => [
                    'local_status',
                    'content',
                    'name'
                ]
            ]
        ];
    }

    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status', 'local_status'], 'integer'],
            [['content'], 'string'],
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqLangs()
    {
        return $this->hasMany(FaqLang::className(), ['owner_id' => 'id']);
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }
}
