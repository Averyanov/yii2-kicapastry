<?php

namespace common\models;

use Yii;
use yii\helpers\VarDumper;
use common\models\CResponser;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $create_date
 * @property string $pay_date
 * @property integer $status
 * @property integer $discount_persent
 * @property string $payment_info
 * @property string $session_info
 * @property string $user_agent
 *
 * @property Cart[] $carts
 * @property User $owner
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_CREATED    = 0;
    const STATUS_SENT       = 1;
    const STATUS_PAID       = 2;
    const STATUS_TRACKED    = 3;
    const STATUS_DELETED    = 4;
    const DISCOUNT_PERSENT    = 50;

    public $status_list = [
        'created',
        'sent',
        'paid',
        'tracked',
        'deleted'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'session_info', 'user_agent'], 'required'],
            [['owner_id', 'status', 'discount_persent'], 'integer'],
            [['create_date', 'pay_date'], 'safe'],
            [['payment_info', 'session_info', 'user_agent'], 'string'],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'create_date' => Yii::t('app', 'Create Date'),
            'pay_date' => Yii::t('app', 'Pay Date'),
            'status' => Yii::t('app', 'Status'),
            'discount_persent' => Yii::t('app', 'Discount Persent'),
            'payment_info' => Yii::t('app', 'Payment Info'),
            'session_info' => Yii::t('app', 'Session Info'),
            'user_agent' => Yii::t('app', 'User Agent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    public function crelink() // :)) create and link to current user
    {
        $this->attributes = [
            'session_info' => CResponser::spongeApi(
                'http://ipgeobase.ru:7020/geo',
                ['ip'   => Yii::$app->request->getUserIP()]
            )->getDocument(),
            'user_agent' => Yii::$app->request->getUserAgent()
        ];
        $user = Yii::$app->user->identity;
        $this->link('owner', $user);

        $this->linkCarts($user);

        return $this;
    }

    public function disableCarts($status)
    {
        foreach ($this->carts as $cart)
        {
            $cart->status = $status;
            $cart->save();
        }

        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();

        return $this;
    }

    public function prepareToPay($status)
    {
        if($status == $this->status)
            return;

        $this
            ->linkCarts()
            ->setStatus($status)
            ->disableCarts($status);
    }

    public function linkCarts()
    {
        $user = $this->owner;
        foreach($user->getCartItems()->where(['status' => 0])->all() as $cart)
            $this->link('carts', $cart);

        return $this;
    }

    public function getCourses()
    {
        return $this->hasMany(
            Course::className(), ['id' => 'course_id']
        )
            ->via('carts');
    }

    public function getTotalCartSum($allow_discount = false)
    {
        $total = $this->getCourses()->sum('price');

        return $allow_discount
            ? ($total - $total * $this->discount_persent / 100)
            : $total;
    }
}
