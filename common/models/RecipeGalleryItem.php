<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "recipe_gallery_item".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $image
 * @property integer $status
 * @property integer $is_main
 *
 * @property Recipe $owner
 */
class RecipeGalleryItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe_gallery_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'image'], 'required'],
            [['owner_id', 'status', 'is_main'], 'integer'],
            [['image'], 'string', 'max' => 128],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'image' => 'Image',
            'status' => 'Status',
            'is_main' => 'Is Main',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Recipe::className(), ['id' => 'owner_id']);
    }
}
