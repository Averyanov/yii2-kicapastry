<?php

namespace common\models;

use common\components\CUrl;
use Yii;
use common\components\ImageBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $name
 * @property string $local
 * @property string $image
 *
 * @property ChefLang[] $chefLangs
 * @property CourseLang[] $courseLangs
 * @property NewsLang[] $newsLangs
 */
class Lang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const DEFAULT_LOCAL = 'ru-RU';

    public
        $width = 30,
        $height = 20;

    public function behaviors()
    {
        return [
            ImageBehavior::className()
        ];
    }

    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'local'], 'required'],
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new"); 
                    }'
            ],
            [['name'], 'string', 'max' => 20],
            [['local'], 'string', 'max' => 5],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'local' => Yii::t('app', 'Local'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    public static function getMap()
    {
        return ArrayHelper::map(static::find()->all(), 'local', 'name');
    }

    public static function getSelectItems()
    {
        return ArrayHelper::getColumn(Lang::find()->all(), function($model){
            $language = $model->local != Lang::DEFAULT_LOCAL ? $model->Clocal : null;

            $isSelectedModel = $model->local == Yii::$app->session['_lang'];

            return [
                'label' => Html::img($model->image).' '.$model->name,
                'url' => !$isSelectedModel
                    ? Url::current(['language' => $language])
                    : null,
                'options' => ['class' => !$isSelectedModel ?: 'disabled'],
            ];
        });
    }

    public static function getCurrent()
    {
        return static::findOne(['local' => Yii::$app->language]);
    }


    // $return_default --- if true method will return local instead empty string
    // when app->language == default local
    public static function getCurrentLocal($return_default = false)
    {
        return (Yii::$app->language != static::DEFAULT_LOCAL || $return_default)
            ? Yii::$app->language
            : '';
    }

    public static function getCurrentCustomizedLocal($return_default = false)
    {
        return (Yii::$app->language != static::DEFAULT_LOCAL || $return_default)
            ? substr(Yii::$app->language,0,-3)
            : '';
    }

//    public function preparePath($extension)
//    {
//        return Yii::$app->params['uploadPathLang'] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getChefLangs()
//    {
//        return $this->hasMany(ChefLang::className(), ['lang_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getCourseLangs()
//    {
//        return $this->hasMany(CourseLang::className(), ['lang_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getNewsLangs()
//    {
//        return $this->hasMany(NewsLang::className(), ['lang_id' => 'id']);
//    }
}
