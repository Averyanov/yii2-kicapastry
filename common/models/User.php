<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;
use common\models\Order;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ADMIN_ID = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by useremail
     *
     * @param string $userimail
     * @return static|null
     */
    public static function findByUseremail($useremail)
    {
        return static::findOne(['email' => $useremail, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    // Get custom user field
    public function getField()
    {
        return $this->hasOne(UserField::className(), ['owner_id' => 'id']);
    }

    public function getPhone()
    {
        return $this->field->phone;
    }

    public function getCartItems()
    {
        return $this->hasMany(Cart::className(), ['owner_id' => 'id']);
    }

    public function getCourses()
    {
        return $this->hasMany(
            Course::className(), ['id' => 'course_id']
        )
            ->via('cartItems');
    }

    public function getTotalCartSum($allow_discount = false)
    {
        $total = $this->getCourses()->sum('price');

        return $allow_discount
            ? ($total - $total * $this->currentOrder->discount_persent / 100)
            : $total;
    }

    public function getFavoriteItems()
    {
        return $this->hasMany(Favorite::className(), ['owner_id' => 'id']);
    }

    public function getFavoriteCourses()
    {
        return $this->hasMany(
            Course::className(), ['id' => 'course_id']
        )
            ->via('favoriteItems');
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['owner_id' => 'id']);
    }

    public function getCurrentOrder()
    {
        return $this->getOrders()->where(['status' => [Order::STATUS_CREATED, Order::STATUS_SENT]])->one();
    }

    public function getDiscount()
    {
        return $this->field->discount;
    }

    public function setDiscount($value)
    {
        $this->field->discount = $this->discount + $value;

        $this->field->save();
    }

    public static function has($id)
    {
        return !Yii::$app->user->identity ? null : !is_null(Yii::$app->user->identity->getCourses()->where(['id' => $id])->one());
    }

}
