<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faq_lang".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $language
 * @property string $content
 *
 * @property Faq $owner
 */
class FaqLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['owner_id', 'language', 'content'], 'required'],
//            [['owner_id'], 'integer'],
//            [['content'], 'string'],
//            [['language'], 'string', 'max' => 6],
//            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'language' => Yii::t('app', 'Language'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Faq::className(), ['id' => 'owner_id']);
    }
}
