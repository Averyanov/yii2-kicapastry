<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery_lang".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $language
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property Gallery $owner
 */
class GalleryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['owner_id', 'language', 'name', 'description', 'meta_title', 'meta_keywords', 'meta_description'], 'required'],
//            [['owner_id'], 'integer'],
//            [['language'], 'string', 'max' => 6],
//            [['name'], 'string', 'max' => 128],
//            [['description', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
//            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'owner_id']);
    }
}
