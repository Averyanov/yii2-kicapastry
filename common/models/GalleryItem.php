<?php

namespace common\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "gallery_item".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $image
 * @property integer $status
 * @property integer $is_main
 *
 * @property Gallery $owner
 */
class GalleryItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'image', 'status', 'is_main'], 'required'],
            [['owner_id', 'status', 'is_main'], 'integer'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'is_main' => Yii::t('app', 'Is Main'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'owner_id']);
    }

    public function setAsMain($id)
    {
        $current = $this->findOne($id);

        if($current)
        {
            $current->is_main = true;
            $previous = $this->find()->where(['and', ['owner_id' => $current->owner_id], ['is_main' => true]])->one();
            if($previous)
            {
                $previous->is_main = 0;
                $previous->save();
            }

            $current->save();
            return true;
        }

        return false;
    }

    public static function setStatus($id)
    {
        $item = self::findOne($id);
        $item->status = $item->status ? 0 : 1;
        return $item->save();
    }

    public function beforeDelete()
    {
        return unlink(ltrim($this->image, '/'));
    }

//    alter table gallery auto_increment = 1;
//    alter table gallery_item auto_increment = 1;
//    alter table gallery_lang auto_increment = 1;
}
