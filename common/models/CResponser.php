<?

namespace common\models;

use yii\helpers\VarDumper;
use DOMDocument;

class CResponser
{
    private $response;

    public static function spongeApi($url, $query)
    {

        $obj = new self();

        $request = implode('',[
            $url,
            '?',
            http_build_query($query)
        ]);
        try
        {
            $obj->response = file_get_contents($request);
        }
        catch (Exception $e)
        {
            echo 'some problem with geoIp';
            var_dump($e);
        }

        return $obj;
    }

    public function getValue($name)
    {
        $response = simplexml_load_string($this->response)->ip;

        return !isset($response->message)
            ? (((array)$response)[$name])
            : null;
    }

    public function getDocument()
    {
        $doc = new DOMDocument();
        $doc->loadXML($this->response);

        return mb_convert_encoding($this->response,"utf-8", "windows-1251");
    }
}