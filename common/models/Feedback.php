<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $username
 * @property string $city
 * @property string $email
 * @property string $mess
 * @property integer $phone
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'mess'], 'required'],
            ['email', 'required', 'when' => function() {
                return $this->phone == null;
            },'whenClient' => "function (attribute, value) {
                return !$('#feedback-phone').val();}",
                'message' => 'оставьте контактные данные для связи(email или телефон)'
            ],
            ['phone', 'required', 'when' => function() {
                return $this->email == null;
            },'whenClient' => "function (attribute, value) {
                return !$('#feedback-email').val();}",
                'message' => 'оставьте контактные данные для связи(email или телефон)'
            ],
            [['mess'], 'string'],
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator', 'country'=>['UA', 'RU']],
            [['username', 'city', 'email'], 'string', 'max' => 255],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'city' => 'City',
            'email' => 'Email',
            'mess' => 'Mess',
            'phone' => 'Phone',
        ];
    }


    public function init()
    {
        $user = Yii::$app->user->identity;

        if($user)
        {
            $this->attributes = [
                'username' => $user->username,
                'email' => $user->email,
                'phone' => $user->field->phone
            ];
        }

        $this->city = CResponser::spongeApi(
            'http://ipgeobase.ru:7020/geo',
            ['ip'   => Yii::$app->request->getUserIP()]
        )
            ->getValue('city');
    }

    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email ?: 'empty@email.dom' => $this->username])
            ->setSubject('some subject')
            ->setTextBody($this->mess)
            ->send();
    }
}
