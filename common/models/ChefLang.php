<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chef_lang".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $language
 * @property string $name
 * @property string $rank
 * @property string $short_description
 * @property string $full_description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property Lang $lang
 * @property Chef $owner
 */
class ChefLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chef_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['name'], 'string', 'max' => 64],
//            [['owner_id', 'language', 'name', 'rank', 'short_description', 'full_description', 'meta_title', 'meta_keywords', 'meta_description'], 'required'],
//            [['owner_id', 'language'], 'string'],
//            [['name'], 'string', 'max' => 64],
//            [['rank', 'full_description', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
//            [['short_description'], 'string', 'max' => 128],
//            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['language' => 'id']],
//            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chef::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'language' => Yii::t('app', 'Lang ID'),
            'name' => Yii::t('app', 'Name'),
            'rank' => Yii::t('app', 'Rank'),
            'short_description' => Yii::t('app', 'Short Description'),
            'full_description' => Yii::t('app', 'Full Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Chef::className(), ['id' => 'owner_id']);
    }
}
