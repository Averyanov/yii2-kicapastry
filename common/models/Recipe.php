<?php

namespace common\models;

use Yii;
//use common\components\ImageBehavior;
use common\components\CMultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;

/**
 * This is the model class for table "recipe".
 *
 * @property integer $id
 * @property string $image
 * @property integer $status
 */
class Recipe extends \yii\db\ActiveRecord
{
    public
        $width = 115,
        $height = 115;

    public function behaviors()
    {
        return [
//            ImageBehavior::className(),
            'ml' => [
                'class' => CMultilingualBehavior::className(),
                'languages' => Lang::getMap(),
                'languageField' => 'language',
                'langClassName' => RecipeLang::className(),
                'langForeignKey' => 'owner_id',
                'tableName' => 'recipe_lang',
                'attributes' => [
                    'name',
                    'description',
                    'short_description',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['status'], 'required'],
            [['status'], 'integer'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
            [['meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 64],
            [['content'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return RecipeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

//    public function preparePath($extension)
//    {
//        return Yii::$app->params['uploadPathRecipe'] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
//    }

    public function getRecipeLangs()
    {
        return $this->hasMany(RecipeLang::className(), ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryItem()
    {
        return $this->hasMany(RecipeGalleryItem::className(), ['owner_id' => 'id']);
    }
}
