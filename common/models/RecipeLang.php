<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "recipe_lang".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $language
 * @property string $name
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $content
 */
class RecipeLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['owner_id', 'language', 'name', 'meta_title', 'meta_keywords', 'meta_description', 'content'], 'required'],
//            [['owner_id'], 'integer'],
//            [['content'], 'string'],
//            [['language'], 'string', 'max' => 6],
//            [['name'], 'string', 'max' => 32],
//            [['meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @inheritdoc
     * @return RecipeLangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecipeLangQuery(get_called_class());
    }
}
