<?

namespace common\models;

use Yii;
use yii\helpers\Url;

class Payment
{
    private $debug;

    public $email, $currency, $url;

    public function __construct($debug = true)
    {
        $this->debug = $debug;
        $this->email = Yii::$app->params['payment_email'];
        $this->currency = 'EUR';
        $this->url = (object)[
            'action' => $this->debug
                ? 'https://www.sandbox.paypal.com/cgi-bin/webscr'
                : 'https://www.paypal.com/cgi-bin/webscr',
            'success' => Url::to(['order/success'], true),
            'return' => 'some_return_url',
            'notify' => 'some_notify_url'
        ];
    }
}