<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>-->
<!--        --><?//= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
<?php Pjax::begin(['enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'owner_id',
                'label' => 'BUYER',
                'value' => function($model){ return $model->owner->username; },
            ],
            [
                'attribute' => 'purchase',
                'label' => 'Purchase',
                'format' => 'html',
                'value' => function($model){
                    return ListView::widget([
                        'dataProvider' => new ActiveDataProvider(['query' => $model->getCourses()]),
                        'layout' => '{items}',
                        'options' => ['tag' => null],
                        'itemOptions' => ['tag' => null],
                        'itemView' => function($mc){
                            return Html::a(
                                $mc->name,
                                Url::toRoute([
                                    'course/view',
                                    'id' => $mc->id
                                ])
                            )." ($mc->price €)".'<br>' ;}
                    ]);
                }
            ],
            [
                'attribute' => 'discount_persent',
                'label' => 'Discount persent',
                'value' => function($model){ return $model->discount_persent.' % from '.$model->getTotalCartSum() . ' €'; }
            ],
            [
                'attribute' => 'sum',
                'label' => 'Total',
                'value' => function($model){
                    $total = $model->getTotalCartSum(true);
                    return Yii::$app->formatter->asDecimal($total).' €';
                },
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){ return $model->status_list[$model->status]; }
            ],
            'create_date',
            'pay_date',
            [
                'label' => 'Payment Method',
                'value' => null
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
