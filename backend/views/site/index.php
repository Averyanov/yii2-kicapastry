<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Мої конгретюляції</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Шефи</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['chef/index']) ?>">Туда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Мови</h2>

                <p>Тут можна додати локалі для сайту [en, ua, ru ....]</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['lang/index']) ?>">Сюда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Мастер-класи</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['course/index']) ?>">Туда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Рецепти</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['recipe/index']) ?>">Сюда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Faq</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['faq/index']) ?>">Туда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Галерея</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['gallery/index']) ?>">Сюда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Покупки</h2>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['order/index']) ?>">Туда &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Словарь</h2>

                <p>Строковые константы [пункты меню, ....]</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute(['translation/index']) ?>">Сюда &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
