<div class="file-thumbnail-footer">
    <div class="file-actions">
        <div class="file-footer-buttons">
            <button type="button" onclick="{MAIN_BTN_LISTENER}" class="kv-file-main btn btn-xs btn-{MAIN_BTN_BG} {MAIN_BTN_CLASS}" title="{MAIN_TITLE}" data-url="/<?= $this->context->id ?>/set-main" data-key="{ID}">
                <i class="glyphicon glyphicon-queen text-{MAIN_ICON_TEXT}"></i>
            </button>
            <button type="button" onclick="{MAIN_BTN_LISTENER}" class="btn btn-xs btn-default" title="Статус: {STATUS_TITLE}" data-url="/<?= $this->context->id ?>/set-status" data-key="{ID}">
                <i class="glyphicon glyphicon-eye-{STATUS_ICON} text-{STATUS_ICON_TEXT}"></i>
            </button>
            {actions}
<!--            <button type="button" class="kv-file-remove btn btn-xs btn-default" title="Удалить файл" data-url="/--><?//= $this->context->id ?><!--/delete-image" data-key="{ID}">-->
<!--                <i class="glyphicon glyphicon-trash text-danger"></i>-->
<!--            </button>-->
<!--            <button type="button" class="kv-file-zoom btn btn-xs btn-default" title="Развернуть">-->
<!--                <i class="glyphicon glyphicon-zoom-in"></i>-->
<!--            </button>-->
        </div>
    </div>
</div>
