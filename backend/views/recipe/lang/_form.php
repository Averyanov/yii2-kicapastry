<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ChefLang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chef-lang-form">

    <?= $form->field($model, 'name'.$lang_prefix)->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content'.$lang_prefix)->textarea(['maxlength' => true, 'rows' => '6']) ?>

    <?= $form->field($model, 'meta_title'.$lang_prefix)->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_keywords'.$lang_prefix)->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description'.$lang_prefix)->textInput(['maxlength' => true]) ?>

</div>
