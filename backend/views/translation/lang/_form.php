<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ChefLang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chef-lang-form">

    <?= $form->field($model, 'content'.$lang_prefix)->textInput(['maxlength' => true]) ?>

</div>
