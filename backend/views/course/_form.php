<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Chef;
use kartik\date\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), $model->getOptions()) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'url')->textInput() ?>

    <?= $form->field($model, 'owner_id')->dropDownList(ArrayHelper::map(Chef::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::className()) ?>

    <?= $this->render('lang/tabs', [
        'items' => $model->getTabs($form),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
