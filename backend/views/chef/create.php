<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Chef */

$this->title = Yii::t('app', 'Create Chef');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chefs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chef-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
