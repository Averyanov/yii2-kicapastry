<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use backend\assets\CFileInputAsset;

CFileInputAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?= $form->field($model, 'status')->checkbox(['checked ' => true]) ?>

    <?= $form->field($model, 'images[]')
        ->widget(
            FileInput::classname(),
            $model->getOptions()
    ) ?>

    <?= $this->render('lang/tabs', [
        'items' => $model->getTabs($form),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'btn_submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
