<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class CFileInputAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/fileInputCustomEvents.js'
    ];
    public $depends = [
        'backend\assets\AppAsset'
    ];
}
