var
    id = null,
    form = $('.gallery-form form'),
    chameleon_button = $('#btn_submit'),
    input = $('#gallery-images'),

    setProp = function (button) {
        $.ajax({
            method: 'post',
            url: button.dataset.url,
            data: {
                id: button.dataset.key
            }
        })
            .done(function(response){
                console.log(response);
                $(input).fileinput('upload');
            })
    },
    uploadAllFiles = function(event, files) {
        $(this).fileinput('upload');
    },
    successUpload = function (event, data) {

        id = data.response.gallery_id;

        chameleon_button
            .removeClass('btn-success')
            .addClass('btn-primary')
            .text('Update');

        form.attr('action', '/gallery/update?id='+id);
    },
    errorUpload = function (event, data, msg) {
        console.log(msg);
    };
    // removeAfterSelect = function (event, code, number) {
    //     // каюсь, я не відав, що творив
    //
    //
    //     console.log(event);
    //     console.log(code);
    //     console.log(number);
    //     console.log('delete');
    //
    //     var
    //         FInput = $(this).attr('data-krajee-fileinput'),
    //         url = window[FInput].deleteUrl;
    //
    //     $.ajax({
    //         method: 'post',
    //         url: url,
    //         data: {
    //             num: number,
    //             id: id
    //         }
    //     })
    //         .done(function (data) {
    //             console.log(data);
    //         })
    // };
    // afterRemoveAllImages = function (event) {
    //     console.log('reset all');
    // };

input
    .on('filebatchselected',        uploadAllFiles)
    .on('filebatchuploadsuccess',   successUpload)
    .on('filebatchuploaderror',     errorUpload);
    // .on('filereset',                afterRemoveAllImages);
    // .on('filepreremove',            removeAfterSelect);
    // .on('fileremoved',            removeAfterSelect)
    // .on('fileclear',            removeAfterSelect)
    // .on('filecleared',            removeAfterSelect)
    // .on('filepredelete',            removeAfterSelect)
    // .on('filedeleted',            removeAfterSelect)
    // .on('filesuccessremove',            removeAfterSelect);



    // .on('fileclear',        function(){console.log('delete2')})
    // .on('filecleared',      function(){console.log('delete3')})
    // .on('filelock',         function(){console.log('delete4')})
    // .on('fileunlock',       function(){console.log('delete5')})
    // .on('filepredelete',    function(){console.log('delete6')})
    // .on('filedeleted',      function(){console.log('delete7')})
    // .on('filesuccessremove', function(){console.log('delete8')})
    // .on('filedisabled',     function(){console.log('delete9')})
    // .on('filezoomhide',     function(){console.log('delete_10')})
    // .on('filecustomerror',  function(event, params, msg) {
    //     console.log('delete_11');
    //     console.log(params.id);
    //     console.log(params.index);
    //     console.log(params.data);
    //     // get message
    //     alert(msg);
    // })
    // .on('fileerror',     function(){console.log('delete_12')})
    // .on('filedeleteerror',     function(){console.log('delete_13')})
    // .on('delete',     function(){console.log('delete_14')})